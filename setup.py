# -*- coding: utf-8 -*-
from setuptools import setup

packages = \
    ['musify', 'musify.scripts']

package_data = \
    {'': ['*']}

install_requires = \
    ['beautifulsoup4>=4.11.1,<5.0.0',
     'envyaml>=1.10.211231,<2.0.0',
     'lxml>=4.9.1,<5.0.0',
     'poetry2setup>=1.1.0,<2.0.0',
     'python-dateutil>=2.8.2,<3.0.0',
     'python-slugify>=6.1.2,<7.0.0',
     'requests>=2.28.1,<3.0.0',
     'spotipy>=2.21.0,<3.0.0',
     'strsimpy>=0.2.1,<0.3.0']

setup_kwargs = {
    'name': 'musify',
    'version': '0.1.0',
    'description': '',
    'long_description': 'None',
    'author': 'Your Name',
    'author_email': 'you@example.com',
    'maintainer': 'None',
    'maintainer_email': 'None',
    'url': 'None',
    'packages': packages,
    'package_data': package_data,
    'install_requires': install_requires,
    'python_requires': '>=3.7,<4.0',
}

setup(**setup_kwargs)

