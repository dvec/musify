from enum import Enum


class Playlist(str, Enum):
    # AMIGA
    RAP_CAVIAR = "37i9dQZF1DX0XUsuxWHRQd"
    RAP_CAVIAR_MEGA = "1cIqSmKay0m0ZjTNr82YOs"

    HIP_HOP_MIXTAPE = "37i9dQZF1DWVdgXTbYm2r0"
    HIP_HOP_MIXTAPE_MEGA = "5yVVkQy84mmaTJJfRHc0w0"

    HIP_HOP_WORLD_STARS = "2Z93pJSgdrBLJuNzfH1Pv6"
    HIP_HOP_WORLD_STARS_MEGA = "2XFxAxlt3tLfcn5AKnZi0u"

    AMIGA = "4a1XmXv1IzyC9xd9JrGhQ3"

    # DEGA
    MODUS_MIO = "37i9dQZF1DX36edUJpD76c"
    MODUS_MIO_MEGA = "4Lxmm2zLTF5tttBBqbNrL8"

    DEUTSCHRAP_ROYAL = "37i9dQZF1DX4TiN7pMwV0Z"
    DEUTSCHRAP_ROYAL_MEGA = "0roRuBc9PxQTWF63bwVnCc"

    DEUTSCHRAP_BRANDNEU = "37i9dQZF1DWSTqUqJcxFk6"
    DEUTSCHRAP_BRANDNEU_MEGA = "104zf89LHK6fIEcO9CZwHs"

    SHISHA_FLOW = "37i9dQZF1DWWmZlZqe008w"
    SHISHA_FLOW_MEGA = "3AtB0EvvxWmtLwF0xXKHOi"

    DEUTSCHTRAP = "37i9dQZF1DX2uJ1OVfn3CH"
    DEUTSCHTRAP_MEGA = "5RNI6I16FCxiMJSMHzUgmb"

    RAP_IN_DEEP = "37i9dQZF1DX0Na9FTYame5"
    RAP_IN_DEEP_MEGA = "0PONnHOyi3VusjDn2ngdTC"

    DEUTSCHRAP_UNTERGRUND = "37i9dQZF1DX1zpUaiwr15A"
    DEUTSCHRAP_UNTERGRUND_MEGA = "42GP46fRyP765VyJ9SHTvp"

    DEUTSCHRAP_MEGA = "5lfevwn5XYbs6zu5Vgoq0z"

    # TOGA
    TODAYS_TOP_HITS = "37i9dQZF1DXcBWIGoYBM5M"
    TODAYS_TOP_HITS_MEGA = "1v1BS0zR6pT9nuLce5FR6E"

    TOP_HITS_2000_2020 = "0KfoUWBrsdwFtcTBNkuEnj"
    TOP_HITS_2000_2020_MEGA = "03jTM2Vs3vjwGmzBpwkcIp"

    TOP_HITS_2000_2020_2 = "7E3uEa1emOcbZJuB8sFXeK"
    TOP_HITS_2000_2020_2_MEGA = "4AHcIrjsucPufqCEHTE6AO"

    TOP_2000_2020_HITS = "71DufkXL7I3nnXSnPcVaUo"
    TOP_2000_2020_HITS_MEGA = "4DWUHd15YNdgAtmmTEHYRn"

    SOFT_POP_HITS = "37i9dQZF1DWTwnEm1IYyoj"
    SOFT_POP_HITS_MEGA = "0zXILke1J8ktfFO3hFdU8B"

    JUST_HITS = "37i9dQZF1DXcRXFNfZr7Tp"
    JUST_HITS_MEGA = "4o5GyDJjcSOKFanftVN2ft"

    TOP_HITS_MEGA = "4vy9EgUa1tr7JZr04Y3CI1"

    # METALIN
    ADRENALIN_WORKOUT_PLUS = "4U0EcHmMnHSRnb1vofYBp5"
    ADRENALIN_WORKOUT_PLUS_MEGA = "7dmegA401iizQ7WlbrYIVR"

    ADRENALIN_WORKOUT = "37i9dQZF1DXe6bgV3TmZOL"
    ADRENALIN_WORKOUT_MEGA = "3hgQto5lQXMyFfDFvtAYEf"

    ADRENALIN_WORKOUT_2 = "3n5URtDwgXFgUie9xjGg35"
    ADRENALIN_WORKOUT_2_MEGA = "3m5uub37fZNF4BSmtxYbur"

    WORKOUT_METAL = "5SpXKOLINelm5Ivr5lUvj8"
    WORKOUT_METAL_MEGA = "4JgF9ypZl0WAl0M43Sh5gq"

    HEAVY_WORKOUT_METAL_HARD_ROCK = "7AyIIH5I3uUP7qKruwnpjn"
    HEAVY_WORKOUT_METAL_HARD_ROCK_MEGA = "2ZPuFPjH09S09SNVzppusE"

    WORKOUT_ROCK = "5c1DEqd2707N9pUffcVhjM"
    WORKOUT_ROCK_MEGA = "6jd37eagZn37jJT79iDRZr"

    WORKOUT_ROCK_METAL = "4XOTNBYOQyp9ymeKjwCEgt"
    WORKOUT_ROCK_METAL_MEGA = "2J3gVveVu6nCe7NiARYVpq"

    # GENERIC
    EVENING_COMMUTE = "37i9dQZF1DX3bSdu6sAEDF"
    EVENING_COMMUTE_MEGA = "7Du6JGi1bPxAraGEcCqbBq"

    HAPPY_HITS = "37i9dQZF1DXdPec7aLTmlC"
    HAPPY_HITS_MEGA = "33k7ubIPW4MBjUsvGC9AWz"

    GET_HOME_HAPPY = "37i9dQZF1DXeby79pVadGa"
    GET_HOME_HAPPY_MEGA = "4QXdH0x2M5aZhS8HQ02Ynb"

    SONGS_SING_SHOWER = "37i9dQZF1DWSqmBTGDYngZ"
    SONGS_SING_SHOWER_MEGA = "7vnytiITO6Tp0AFHEtu8eV"

    TOP_50_GLOBAL = "37i9dQZEVXbMDoHDwVN2tF"
    TOP_50_GLOBAL_MEGA = "4vMOZstRB2qwWUXdJrix6L"

    SONGS_SING_CAR = "37i9dQZF1DWWMOmoXKqHTD"
    SONGS_SING_CAR_MEGA = "3NhCiE7Uxmg0dvNSGWm0S6"

    MORNING_COMMUTE = "37i9dQZF1DX2MyUCsl25eb"
    MORNING_COMMUTE_MEGA = "4SEx5FNrBR4ZtNVmaX4XFW"

    GENERIC_MEGA = "69OodXeAShLXIILiIssvNQ"

    # MISC
    CHILL_HITS = "37i9dQZF1DX4WYpdgoIcn6"
    CHILL_HITS_MEGA = "0w0wlDl4rexP8Xwgs5YSZH"

    GYM_MOTIVATION_2021 = "5cAwvaSXeNSrSbmrOUSBzo"
    GYM_MOTIVATION_2021_MEGA = "4z45l11ssRzFl5iNJtVafS"

    GYM_ROCK_MUSIC = "5w6VLpEf4yquKQleXsK8Af"
    GYM_ROCK_MUSIC_MEGA = "355gTh0e8SGWXkRyWAEt5S"

    ESL_ONE_CSGO_OFFICIAL_SOUNDTRACK = "5twuCRBQQ1mSKFjll6oFxP"
    ESL_ONE_CSGO_OFFICIAL_SOUNDTRACK_MEGA = "3M28LYez24hkmYbd5eDjNc"

    ESL_PRO_LEAGUE_OFFICIAL_SOUNDTRACK = "00baB9PVWDBii7jtGvrPCs"
    ESL_PRO_LEAGUE_OFFICIAL_SOUNDTRACK_MEGA = "5TGDgPUEpWNceZ9WRGF4pZ"

    HAGEN_CLASSICS = "4ePSsPyWUzxjbGHZcvKOOr?si=a6068fd040504de4"
    HAGEN_CLASSICS_MEGA = "2Jtfz6DqHpo9f7c3mW39sK?si=fc89cf6363154d53"

    STEGI_DER_SHIT = "2VrGiFrq6557ImGVo8Ud8S?si=9c2ee5af855744a1"
    STEGI_DER_SHIT_MEGA = "4R8LtpKA9mdqtyocgp8gTG?si=80a384061c714861"

    # FVCKBOI
    FVCKBOI = "65fr1aWmt2kdHNXzu4CqRD"
    FVCKBOI_MEGA = "6nttWws5X9B8BTyBbSQ0aj"

    FVCKBOI_NON_DE = "0ZrpxiP6rdW8EMPfWucq0A"
    FVCKBOI_NON_DE_UP_TO_DATE = "4YCtWlmx6Zt3xDQQFM8zqV"
    FVCKBOI_DE = "4tAizEHKp2m9zE7MUT4iCf"
    FVCKBOI_DE_UP_TO_DATE = "3ZgcrUd1YRWMATylVles6F"
    FVCKBOI_UNMATCHED = "4t7osP4WmFXCOFXohXboH6"

    # COLLECT
    TOP_SONGS_GLOBAL = "37i9dQZEVXbNG2KDcFcKOF"
    TOP_SONGS_GLOBAL_MEGA = "79geaOkzRy9LzyHZE57o7j"

    POP_RISING = "37i9dQZF1DWUa8ZRTfalHk"
    POP_RISING_MEGA = "1BzvvpO0ZRk5JW2PnEerr1"

    SINGLED_OUT = "37i9dQZF1DX8f6LHxMjnzD"
    SINGLED_OUT_MEGA = "6FpVyiln5lcT0iZXJ5iYek"

    FRESH_CHILL = "37i9dQZF1DX5CdVP4rz81C"
    FRESH_CHILL_MEGA = "5DoU51MSLf1VVrZihCD2hE"

    SONGS_SCREAM_CAR = "37i9dQZF1DX4mWCZw6qYIw"
    SONGS_SCREAM_CAR_MEGA = "15ixVpS4fnHyC6ykmHE6iY"

    MEGA_HIT_MIX = "37i9dQZF1DXbYM3nMM0oPk"
    MEGA_HIT_MIX_MEGA = "66ZLaKcdpWar69eOTm7kzl"

    HIT_REWIND = "37i9dQZF1DX0s5kDXi1oC5"
    HIT_REWIND_MEGA = "0PZ9Po3C0hHlwsGliTJqOK"

    ROCK_THIS = "37i9dQZF1DXcF6B6QPhFDv"
    ROCK_THIS_MEGA = "2aRucdbHWn5dJNnaqwGkHU"

    # FRESH FINDS
    FRESH_FINDS_POP = "37i9dQZF1DX3u9TSHqpdJC"
    FRESH_FINDS_POP_MEGA = "3CRVZv9mbegboSo8B1Sqkv"

    FRESH_FINDS = "37i9dQZF1DWWjGdmeTyeJ6"
    FRESH_FINDS_MEGA = "0tDHeH0xqPP9bE2TcHeWHf"

    FRESH_FINDS_COUNTRY = "37i9dQZF1DWYUfsq4hxHWP"
    FRESH_FINDS_COUNTRY_MEGA = "4cfjsVXA9v4dJS47F4vCt1"

    FRESH_FINDS_SG_MY = "37i9dQZF1DWWvrRGuT6dlZ"
    FRESH_FINDS_SG_MY_MEGA = "7c7pAj8IuL1MaqenbnZAOy"

    FRESH_FINDS_ROCK = "37i9dQZF1DX78toxP7mOaJ"
    FRESH_FINDS_ROCK_MEGA = "691pfVKKEaMerKaEZJuyBI"

    FRESH_FINDS_EXPERIMENTAL = "37i9dQZF1DX8C585qnMYHP"
    FRESH_FINDS_EXPERIMENTAL_MEGA = "2EWih7kVdUfr0eSlNhToII"

    FRESH_FINDS_INDIE = "37i9dQZF1DWT0upuUFtT7o"
    FRESH_FINDS_INDIE_MEGA = "5wtYc0Bj3XRCBZDpYEMDxN"

    FRESH_FINDS_BASEMENT = "37i9dQZF1DX6bBjHfdRnza"
    FRESH_FINDS_BASEMENT_MEGA = "4PVpqc2Z45AYYHLyMA5orm"

    FRESH_FINDS_WAVE = "37i9dQZF1DWUFAJPVM3HTX"
    FRESH_FINDS_WAVE_MEGA = "5UFwxNWvjcqjpyWs7CmVtR"

    FRESH_FINDS_HIP_HOP = "37i9dQZF1DWW4igXXl2Qkp"
    FRESH_FINDS_HIP_HOP_MEGA = "5Qec7fyYqV9OYwb53ClBYa"

    # RADIO STATIONS
    VIRGIN_ROCK = "5B9l4tLNCDwP2AY2kZhZUc"
    VIRGIN_RADIO = "3VYAkuDMGupq3f7wu6S0iF"
    RADIO_SWISS_POP = "49swf3RjAK2mEGbbkUtD8P"

    ENERGY_ZUERICH = "6SyYv9QD8d5zY4OpAuZzCe"
    ENERGY_ROCK = "0dHH4KRSIVLQy1g0aBJG2B"
    ENERGY_TOP_30 = "1AWsF4pJo6MSNh4KGSUIF3"
    ENERGY_AT_WORK = "5f7ykgnhXPKl8Vn9zknEt2"
    ENERGY_HITS = "2cutQMA1SGOMrkIpYtGNiG"
    ENERGY_00S = "0xYdNLUz0sdC6ohyezr5pG"
    ENERGY_10S = "1ABe9iViX5VlOPAlYeFqvS"
    ENERGY_80S = "1SaVViFpop2Q3JOj6b8xox"
    ENERGY_90S = "2YFmanOql559EfZNrkyHsO"
    ENERGY_GERMAN = "5y99EcxsSegTSwKewnTfVl"
    ENERGY_LOUNGE = "3nMA7yll9JnFLT8UC6qdw5"
    ENERGY_LOVE = "1PzCfRDYbL4JqwQFGACDmo"
    ENERGY_METIME = "5e3lN2KiBQJtjAtbMc0XkR"
    ENERGY_PARTYVIBES = "2uZeQcpJAkpj9byfTXtxXZ"
    ENERGY_POP = "2FFREZ78T6fl8aNMG8gg08"
    ENERGY_SPECIAL3 = "1ULZ4LTVIIfC9XarritrPT"
    ENERGY_STRASSENRAP = "6A5ORuTBWzzlcfMYX6jyNu"
    ENERGY_SUMMERVIBES = "6gZ4UUUM6ptfjrMNVjxYQ6"
    ENERGY_URBAN = "4Ysn0Cd93JslricqEFbeeK"
    ENERGY_WORKOUT = "6vXJF0N8nimApcGIXvqXSy"
    ENERGY_WOMXN = "54b1Fz4ttLkEGrpJBUEq6L"

    GYM_MOTIVATION_WORKOUT_MUSIC = "12xgLeTIUrC5A6UhjHQoch"
    GYM_MOTIVATION_WORKOUT_MUSIC_MEGA = "50sb1bVOwCIZml5wz8l1hp"

    # ROCK ANTHEMS
    ROCK_ANTHEMS_60 = "37i9dQZF1DWWzBc3TOlaAV"
    ROCK_ANTHEMS_70 = "37i9dQZF1DWWwzidNQX6jx"
    ROCK_ANTHEMS_80 = "37i9dQZF1DX1spT6G94GFC"
    ROCK_ANTHEMS_90 = "37i9dQZF1DX1rVvRgjX59F"
    ROCK_ANTHEMS_00 = "37i9dQZF1DX3oM43CtKnRV"
    ROCK_ANTHEMS_10 = "37i9dQZF1DX99DRG9N39X3"
    ROCK_ANTHEMS_MEGA = "61d9AHDVRWKxOseIKbOAFd"

    # ROCK MISC
    ROCK_CLASSICS = "37i9dQZF1DWXRqgorJj26U"
    ROCK_CLASSICS_MEGA = "3I7Vt0RduWPB9mVJyBLepj"

    ROCK_ROTATION = "37i9dQZF1DWZJhOVGWqUKF"
    ROCK_ROTATION_MEGA = "0J0mVPGbdFAqdbfJl6lgDK"

    ROCK_PARTY = "37i9dQZF1DX8FwnYE6PRvL"
    ROCK_PARTY_MEGA = "1l5MBG6NP40nebFQ0kdU8l"

    ROCKHYMNEN = "37i9dQZF1DX4vth7idTQch"
    ROCKHYMNEN_MEGA = "4zBZtK2kDWZo3MIZ14LsWQ"

    ROCK_SOLID = "37i9dQZF1DX49jUV2NfGku"
    ROCK_SOLID_MEGA = "3AY2R2Bmeia58MKnZuaanO"

    ROCK_MIX_MEGA = "4f3m5OEamwnLyple0Z5l8Q"

    # ALL OUT
    ALL_OUT_60S = "37i9dQZF1DXaKIA8E7WcJj"
    ALL_OUT_60S_MEGA = "0wzx9GjEfyL4UP3OtCI95n"

    ALL_OUT_70S = "37i9dQZF1DWTJ7xPn4vNaz"
    ALL_OUT_70S_MEGA = "766WSUt36vePmA9JPwDoyA"

    ALL_OUT_80S = "37i9dQZF1DX4UtSsGT1Sbe"
    ALL_OUT_80S_MEGA = "30KzNXcLK3weA7ze0QHtQW"

    ALL_OUT_90S = "37i9dQZF1DXbTxeAdrVG2l"
    ALL_OUT_90S_MEGA = "32kh7YCDwtLk34ygXj8152"

    ALL_OUT_2000S = "37i9dQZF1DX4o1oenSJRJd"
    ALL_OUT_2000S_MEGA = "0zVcnl7mGJJOY9lcCO4hK2"

    ALL_OUT_2010S = "37i9dQZF1DX5Ejj0EkURtP"
    ALL_OUT_2010S_MEGA = "05jNKV8Y1KDKjWCdTrnU6T"

    # TOP HITS OF
    TOP_HITS_OF_1970 = "37i9dQZF1DWXQyLTHGuTIz"
    TOP_HITS_OF_1970_MEGA = "3I1FZV1RAktF50w9QHrnZt"

    TOP_HITS_OF_1971 = "37i9dQZF1DX43B4ApmA3Ee"
    TOP_HITS_OF_1971_MEGA = "6Fj3YpbUhBnvkbQWOctpHh"

    TOP_HITS_OF_1972 = "37i9dQZF1DXaQBa5hAMckp"
    TOP_HITS_OF_1972_MEGA = "6HB2fDphFute8j2TC44Y2z"

    TOP_HITS_OF_1973 = "37i9dQZF1DX2ExTChOnD3g"
    TOP_HITS_OF_1973_MEGA = "7F1aIQ5k1Gba8cEd01SzdF"

    TOP_HITS_OF_1974 = "37i9dQZF1DWVg6L7Yq13eC"
    TOP_HITS_OF_1974_MEGA = "1rJtqzdHrInwRxwNeXQIGx"

    TOP_HITS_OF_1975 = "37i9dQZF1DX3TYyWu8Zk7P"
    TOP_HITS_OF_1975_MEGA = "1OYgFuKM72Gi3gyX4f1Ljg"

    TOP_HITS_OF_1976 = "37i9dQZF1DX6rhG68uMHxl"
    TOP_HITS_OF_1976_MEGA = "5zdorT7yg3lLa6VLBXMPAg"

    TOP_HITS_OF_1977 = "37i9dQZF1DX26cozX10stk"
    TOP_HITS_OF_1977_MEGA = "1ilrjNVahPqIMMCd7UDd2i"

    TOP_HITS_OF_1978 = "37i9dQZF1DX0fr2A59qlzT"
    TOP_HITS_OF_1978_MEGA = "5oY60awYFiIpEwDLyBVRCV"

    TOP_HITS_OF_1979 = "37i9dQZF1DWZLO9LcfSmxX"
    TOP_HITS_OF_1979_MEGA = "6nhCSadkpTMaJMXntfYoqN"

    TOP_HITS_OF_1980 = "37i9dQZF1DWXbLOeOIhbc5"
    TOP_HITS_OF_1980_MEGA = "0ebgatRWey0hYQ80rHGCMA"

    TOP_HITS_OF_1981 = "37i9dQZF1DX3MaR62kDrX7"
    TOP_HITS_OF_1981_MEGA = "3NMmahwGJkisknkYLEOj7Y"

    TOP_HITS_OF_1982 = "37i9dQZF1DXas7qFgKz9OV"
    TOP_HITS_OF_1982_MEGA = "7ECmC3xLiidHobrgtR09bj"

    TOP_HITS_OF_1983 = "37i9dQZF1DXbE3rNuDfpVj"
    TOP_HITS_OF_1983_MEGA = "7u5JmiHwHxj9qlkYi8Fk13"

    TOP_HITS_OF_1984 = "37i9dQZF1DX2O7iyPnNKby"
    TOP_HITS_OF_1984_MEGA = "0n6GQBhDS3LYsxjMEWuwoh"

    TOP_HITS_OF_1985 = "37i9dQZF1DWXZ5eJ1sVtmf"
    TOP_HITS_OF_1985_MEGA = "2xpBQXdH3bsREPESuZakwx"

    TOP_HITS_OF_1986 = "37i9dQZF1DX7b12kdMQTpG"
    TOP_HITS_OF_1986_MEGA = "3duNr9oNeFWmt3TP9vTDiL"

    TOP_HITS_OF_1987 = "37i9dQZF1DX38yySwWsFRT"
    TOP_HITS_OF_1987_MEGA = "72s8dLb5S4kP30vjTNF1dq"

    TOP_HITS_OF_1988 = "37i9dQZF1DX3MZ9dVGvZnZ"
    TOP_HITS_OF_1988_MEGA = "3eoWnJbDLRIV32CT3nzA24"

    TOP_HITS_OF_1989 = "37i9dQZF1DX4qJrOCfJytN"
    TOP_HITS_OF_1989_MEGA = "3G2kxnPtipmf2J9uVX0fxd"

    TOP_HITS_OF_1990 = "37i9dQZF1DX4joPVMjBCAo"
    TOP_HITS_OF_1990_MEGA = "1LaPLyhRrkXi8IilesxeCE"

    TOP_HITS_OF_1991 = "37i9dQZF1DX6TtJfRD994c"
    TOP_HITS_OF_1991_MEGA = "65UtvR6pFKjCVnC7axg2mv"

    TOP_HITS_OF_1992 = "37i9dQZF1DX9ZZCtVNwklG"
    TOP_HITS_OF_1992_MEGA = "385EX17mdXQBwF2NdDwFsT"

    TOP_HITS_OF_1993 = "37i9dQZF1DXbUFx5bcjwWK"
    TOP_HITS_OF_1993_MEGA = "5uy4LaUQU0Yh4q3MQ5zsAF"

    TOP_HITS_OF_1994 = "37i9dQZF1DXbKFudfYGcmj"
    TOP_HITS_OF_1994_MEGA = "6aqjZOkeyVD6997lYEawlE"

    TOP_HITS_OF_1995 = "37i9dQZF1DXayIOFUOVODK"
    TOP_HITS_OF_1995_MEGA = "4LsPYvYmKgcgT7CYdNeDgh"

    TOP_HITS_OF_1996 = "37i9dQZF1DWZkDl55BkJmo"
    TOP_HITS_OF_1996_MEGA = "2F81vz0ly0nTjID6lBemW1"

    TOP_HITS_OF_1997 = "37i9dQZF1DWWKd15PHZNnl"
    TOP_HITS_OF_1997_MEGA = "4Iqfj3CshDZXTiTGU7oaOr"

    TOP_HITS_OF_1998 = "37i9dQZF1DWWmGB2u14f8m"
    TOP_HITS_OF_1998_MEGA = "5v11WRRvMtKfqX4kMzHBMR"

    TOP_HITS_OF_1999 = "37i9dQZF1DX4PrR66miO50"
    TOP_HITS_OF_1999_MEGA = "18w3kdlFAp0RKF7FdPJJhn"

    TOP_HITS_OF_2000 = "37i9dQZF1DWUZv12GM5cFk"
    TOP_HITS_OF_2000_MEGA = "2RbE0WfrBudXi0EoXovB9E"

    TOP_HITS_OF_2001 = "37i9dQZF1DX9Ol4tZWPH6V"
    TOP_HITS_OF_2001_MEGA = "7swtYn6HesHVBXErdtD5o0"

    TOP_HITS_OF_2002 = "37i9dQZF1DX0P7PzzKwEKl"
    TOP_HITS_OF_2002_MEGA = "0DAsCVegL0qY8mpOA2hbrb"

    TOP_HITS_OF_2003 = "37i9dQZF1DXaW8fzPh9b08"
    TOP_HITS_OF_2003_MEGA = "0j0meAV6G79XpqY77M2v54"

    TOP_HITS_OF_2004 = "37i9dQZF1DWTWdbR13PQYH"
    TOP_HITS_OF_2004_MEGA = "3hSUgWcsGWVWcQdq0n1l1u"

    TOP_HITS_OF_2005 = "37i9dQZF1DWWzQTBs5BHX9"
    TOP_HITS_OF_2005_MEGA = "4d8TNZ5eMMPOAYlNIyXVwj"

    TOP_HITS_OF_2006 = "37i9dQZF1DX1vSJnMeoy3V"
    TOP_HITS_OF_2006_MEGA = "41pHBX61DIHNTIsXWBs9ff"

    TOP_HITS_OF_2007 = "37i9dQZF1DX3j9EYdzv2N9"
    TOP_HITS_OF_2007_MEGA = "47qzdpDublMoG122IDoSu4"

    TOP_HITS_OF_2008 = "37i9dQZF1DWYuGZUE4XQXm"
    TOP_HITS_OF_2008_MEGA = "4Jn7gTdxrgBkC0yA0FbJoP"

    TOP_HITS_OF_2009 = "37i9dQZF1DX4UkKv8ED8jp"
    TOP_HITS_OF_2009_MEGA = "0NbM6MuAHL9yvQ4t4jUvfp"

    TOP_HITS_OF_2010 = "37i9dQZF1DXc6IFF23C9jj"
    TOP_HITS_OF_2010_MEGA = "52VqRZn8gOvV0OeJHB6AAJ"

    TOP_HITS_OF_2011 = "37i9dQZF1DXcagnSNtrGuJ"
    TOP_HITS_OF_2011_MEGA = "2ImsStJcCO7ZYPpSqnK7vC"

    TOP_HITS_OF_2012 = "37i9dQZF1DX0yEZaMOXna3"
    TOP_HITS_OF_2012_MEGA = "6VNtLpLpXCTq9ea14hMVb9"

    TOP_HITS_OF_2013 = "37i9dQZF1DX3Sp0P28SIer"
    TOP_HITS_OF_2013_MEGA = "0vanDactd754yCZAaAfcam"

    TOP_HITS_OF_2014 = "37i9dQZF1DX0h0QnLkMBl4"
    TOP_HITS_OF_2014_MEGA = "6F7WowBhBBnvgvTHczd4Lv"

    TOP_HITS_OF_2015 = "37i9dQZF1DX9ukdrXQLJGZ"
    TOP_HITS_OF_2015_MEGA = "1Mzx5wT6VFjPENBCHJl1qt"

    TOP_HITS_OF_2016 = "37i9dQZF1DX8XZ6AUo9R4R"
    TOP_HITS_OF_2016_MEGA = "4iISchvD6uRoxsW4WpJ4kP"

    TOP_HITS_OF_2017 = "37i9dQZF1DWTE7dVUebpUW"
    TOP_HITS_OF_2017_MEGA = "4qeNYxRgG2zasVvP18JpXb"

    TOP_HITS_OF_2018 = "37i9dQZF1DXe2bobNYDtW8"
    TOP_HITS_OF_2018_MEGA = "64VppfgiUKMxVRg15INGQs"

    TOP_HITS_OF_2019 = "37i9dQZF1DWVRSukIED0e9"
    TOP_HITS_OF_2019_MEGA = "7tztAbnlW5c8nBhPay5iGn"

    TOP_HITS_OF_1970S_COLLECTION = "2vK6iGUxNAvKr2RnFsGrZA"
    TOP_HITS_OF_1980S_COLLECTION = "4Vai6q22pPTmc2yMbZs5sb"
    TOP_HITS_OF_1990S_COLLECTION = "3lFBF2z6EqR4yrWDk5ge07"
    TOP_HITS_OF_2000S_COLLECTION = "07DtUVnosSV8JWbUGP5eUK"
    TOP_HITS_OF_2010S_COLLECTION = "10oaHSyUPqWzIF0PPCAkzC"
    TOP_HITS_OF_2000_TO_2019_COLLECTION = "0xrp3TqmF23dpG6fKbvSie"

    TOP_HITS_OF_MY_TIME_COLLECTION = "1H0rYUCxwF4nz6W1nH9ERH"
    TOP_HITS_OF_MY_TIME_2_COLLECTION = "5LwLzc3Ag7wFGJKBXUbcZj"

    # TOP HITS DEUTSCHLAND
    TOP_HITS_DEUTSCHLAND_1980 = "37i9dQZF1DX7px7bi3Of9w"
    TOP_HITS_DEUTSCHLAND_1980_MEGA = "0CV9K0dLUcTqkjesfg6Cqv"

    TOP_HITS_DEUTSCHLAND_1981 = "37i9dQZF1DWUEahILum3Hc"
    TOP_HITS_DEUTSCHLAND_1981_MEGA = "4axuYUlZXvN0jo8wMxb9mD"

    TOP_HITS_DEUTSCHLAND_1982 = "37i9dQZF1DX8sFebZKe3ZN"
    TOP_HITS_DEUTSCHLAND_1982_MEGA = "5vtvGTrXuYKig6B94ZfYsa"

    TOP_HITS_DEUTSCHLAND_1983 = "37i9dQZF1DX0M1lCL57WtY"
    TOP_HITS_DEUTSCHLAND_1983_MEGA = "3hTyDGoXvgR6laK1uOhhuz"

    TOP_HITS_DEUTSCHLAND_1984 = "37i9dQZF1DXdeVKi7xJiaZ"
    TOP_HITS_DEUTSCHLAND_1984_MEGA = "0Dppz6j26qTT1YMoCUzzTs"

    TOP_HITS_DEUTSCHLAND_1985 = "37i9dQZF1DX0gFv8cNMxdS"
    TOP_HITS_DEUTSCHLAND_1985_MEGA = "36SzOY8Mn7KyTxT9SdYMmr"

    TOP_HITS_DEUTSCHLAND_1986 = "37i9dQZF1DX2Z9oizXiqw7"
    TOP_HITS_DEUTSCHLAND_1986_MEGA = "74o9Sp4sFmZ8OPJNmkRVRc"

    TOP_HITS_DEUTSCHLAND_1987 = "37i9dQZF1DX79UYDqTItDL"
    TOP_HITS_DEUTSCHLAND_1987_MEGA = "3bTOyQSrOVvKeSqCeD8LEs"

    TOP_HITS_DEUTSCHLAND_1988 = "37i9dQZF1DWUDRTidgvCYu"
    TOP_HITS_DEUTSCHLAND_1988_MEGA = "55jQdVEdDBak3GBjhYSkKX"

    TOP_HITS_DEUTSCHLAND_1989 = "37i9dQZF1DX0PDiLYcpHNR"
    TOP_HITS_DEUTSCHLAND_1989_MEGA = "7hZj6JcyUmBjZbJ6h7GI33"

    TOP_HITS_DEUTSCHLAND_1990 = "37i9dQZF1DXcPN8EovlKt3"
    TOP_HITS_DEUTSCHLAND_1990_MEGA = "10WQJskSSUtK8PuCJcfnOC"

    TOP_HITS_DEUTSCHLAND_1991 = "37i9dQZF1DX0vcWxA5Joqk"
    TOP_HITS_DEUTSCHLAND_1991_MEGA = "6RU0YGvBUWgGQJDzQ7LYiJ"

    TOP_HITS_DEUTSCHLAND_1992 = "37i9dQZF1DWW2Qscknu9Uy"
    TOP_HITS_DEUTSCHLAND_1992_MEGA = "1HxAMhcTvK9Bodr0rvUHju"

    TOP_HITS_DEUTSCHLAND_1993 = "37i9dQZF1DWYfs0Eb7UEFD"
    TOP_HITS_DEUTSCHLAND_1993_MEGA = "4IMgFTORMF50dsguM4fvwJ"

    TOP_HITS_DEUTSCHLAND_1994 = "37i9dQZF1DX5VlkJPoLHIU"
    TOP_HITS_DEUTSCHLAND_1994_MEGA = "1FtSlmX2ziReWvZseuLPBS"

    TOP_HITS_DEUTSCHLAND_1995 = "37i9dQZF1DX1hMWbYNRDUE"
    TOP_HITS_DEUTSCHLAND_1995_MEGA = "5hK7LAUgO7v59dYfNYwPsU"

    TOP_HITS_DEUTSCHLAND_1996 = "37i9dQZF1DWW2hPRmLK5MU"
    TOP_HITS_DEUTSCHLAND_1996_MEGA = "0tEFEkrUXx8k6I2k51saC0"

    TOP_HITS_DEUTSCHLAND_1997 = "37i9dQZF1DXawlZtzXcMwZ"
    TOP_HITS_DEUTSCHLAND_1997_MEGA = "1hmfaTBP9BPE9X1V3fZwVR"

    TOP_HITS_DEUTSCHLAND_1998 = "37i9dQZF1DXe5uQyyOUpyO"
    TOP_HITS_DEUTSCHLAND_1998_MEGA = "3tDeM3huTJUmmtcNMQWXFq"

    TOP_HITS_DEUTSCHLAND_1999 = "37i9dQZF1DWWmtnQChogUP"
    TOP_HITS_DEUTSCHLAND_1999_MEGA = "5ykydfbAR3qI28E1mwHADk"

    TOP_HITS_DEUTSCHLAND_2000 = "37i9dQZF1DXa6lrldix0iL"
    TOP_HITS_DEUTSCHLAND_2000_MEGA = "27OGNJNmODwJBeILDeLw65"

    TOP_HITS_DEUTSCHLAND_2001 = "37i9dQZF1DXaUgNafjx16Y"
    TOP_HITS_DEUTSCHLAND_2001_MEGA = "3Z68xLZnUtRjJWwmNo7LuE"

    TOP_HITS_DEUTSCHLAND_2002 = "37i9dQZF1DXdsthAc5ncNc"
    TOP_HITS_DEUTSCHLAND_2002_MEGA = "5O1Yl9JRkL6J72IzjBNVlM"

    TOP_HITS_DEUTSCHLAND_2003 = "37i9dQZF1DWXSg0F6aKutx"
    TOP_HITS_DEUTSCHLAND_2003_MEGA = "0AI3IIQTGFdFme4FPplIal"

    TOP_HITS_DEUTSCHLAND_2004 = "37i9dQZF1DX9bmAYCF0dF3"
    TOP_HITS_DEUTSCHLAND_2004_MEGA = "5mJbSFZbJKMv5jYSKcEen5"

    TOP_HITS_DEUTSCHLAND_2005 = "37i9dQZF1DX8qf42uKbNNo"
    TOP_HITS_DEUTSCHLAND_2005_MEGA = "4h97WmrysZKO9NpuBHq5fh"

    TOP_HITS_DEUTSCHLAND_2006 = "37i9dQZF1DX7LHV2kQlJGY"
    TOP_HITS_DEUTSCHLAND_2006_MEGA = "7uNAGwbDHzyTW1Zs6ZELfT"

    TOP_HITS_DEUTSCHLAND_2007 = "37i9dQZF1DWUtdrERS5A2r"
    TOP_HITS_DEUTSCHLAND_2007_MEGA = "5Er8svwkH5xCQZVIHv3Wy0"

    TOP_HITS_DEUTSCHLAND_2008 = "37i9dQZF1DX6Kp84BK9c46"
    TOP_HITS_DEUTSCHLAND_2008_MEGA = "2CSQgh1JZsRgNZX1iZFom6"

    TOP_HITS_DEUTSCHLAND_2009 = "37i9dQZF1DXcgnmX5S5i6T"
    TOP_HITS_DEUTSCHLAND_2009_MEGA = "3MWOCdfkH8eLogHDZK6BBu"

    TOP_HITS_DEUTSCHLAND_2010 = "37i9dQZF1DWSveSxooJxic"
    TOP_HITS_DEUTSCHLAND_2010_MEGA = "0tWG7RYjv3bmZwhXiHg2t0"

    TOP_HITS_DEUTSCHLAND_2011 = "37i9dQZF1DX9ayFuTbByR2"
    TOP_HITS_DEUTSCHLAND_2011_MEGA = "7gXIrdPASZN9iyFF2AsAB4"

    TOP_HITS_DEUTSCHLAND_2012 = "37i9dQZF1DX1QTn463W5dY"
    TOP_HITS_DEUTSCHLAND_2012_MEGA = "0k27TtVItjo2ntzMpHqsv0"

    TOP_HITS_DEUTSCHLAND_2013 = "37i9dQZF1DX978QtjVD2M2"
    TOP_HITS_DEUTSCHLAND_2013_MEGA = "0fD9AqbS4GZHMRRW24KKfi"

    TOP_HITS_DEUTSCHLAND_2014 = "37i9dQZF1DX6GS9MMVwjK8"
    TOP_HITS_DEUTSCHLAND_2014_MEGA = "5b8QkrWbjCpo1M5uci31RQ"

    TOP_HITS_DEUTSCHLAND_2015 = "37i9dQZF1DXdFAwRvbyB0a"
    TOP_HITS_DEUTSCHLAND_2015_MEGA = "6CEKqRlTS8dXHqwRU1mxrk"

    TOP_HITS_DEUTSCHLAND_2016 = "37i9dQZF1DX2gpe9xDc5Yy"
    TOP_HITS_DEUTSCHLAND_2016_MEGA = "39Nb43QVs4FGld24hcGjTQ"

    TOP_HITS_DEUTSCHLAND_2017 = "37i9dQZF1DX2gZ93CcvwLK"
    TOP_HITS_DEUTSCHLAND_2017_MEGA = "5WFZUOzY8WRu5BcACEJkNo"

    TOP_HITS_DEUTSCHLAND_2018 = "37i9dQZF1DWV86bXjNWiue"
    TOP_HITS_DEUTSCHLAND_2018_MEGA = "64ZUk0DdFYF8Jo2A0xW2gA"

    TOP_HITS_DEUTSCHLAND_2019 = "37i9dQZF1DX1kdxFVBZxbf"
    TOP_HITS_DEUTSCHLAND_2019_MEGA = "3q3cpzYp2UIEXwRstyGoMS"

    TOP_HITS_DEUTSCHLAND_1980S_COLLECTION = "616Ei4AJ600MSExfxWDpAm"
    TOP_HITS_DEUTSCHLAND_1990S_COLLECTION = "25bebWSd8othAHpzFwylHE"
    TOP_HITS_DEUTSCHLAND_2000S_COLLECTION = "1P3Oy985JfbEUT2edvMYRH"
    TOP_HITS_DEUTSCHLAND_2010S_COLLECTION = "25NK5GFujB795t5NlM5fja"
    TOP_HITS_DEUTSCHLAND_2000_TO_2019_COLLECTION = "7gdYWuUWoc0I7FKpt3Anba"

    # BEST RAP SONGS OF

    BEST_RAP_SONGS_OF_1987 = "37i9dQZF1DX7FKmscS2x8B"
    BEST_RAP_SONGS_OF_1987_MEGA = "5KiOxl9xNDZ0goHiPA3GTt"

    BEST_RAP_SONGS_OF_1988 = "37i9dQZF1DWXL4gG0TAH6S"
    BEST_RAP_SONGS_OF_1988_MEGA = "7EU30FYLH004nOumpMZCwJ"

    BEST_RAP_SONGS_OF_1989 = "37i9dQZF1DX5qDhvewvOhw"
    BEST_RAP_SONGS_OF_1989_MEGA = "1ZS5UTFVkQnJNLd1NB0MEe"

    BEST_RAP_SONGS_OF_1990 = "37i9dQZF1DWW45yEGjKTLP"
    BEST_RAP_SONGS_OF_1990_MEGA = "4HPA4VAmIWFSidhEKm2xl1"

    BEST_RAP_SONGS_OF_1991 = "37i9dQZF1DX3zKvS2WqJNQ"
    BEST_RAP_SONGS_OF_1991_MEGA = "50Pzl2O6Sh0R0pHcfrEfJN"

    BEST_RAP_SONGS_OF_1992 = "37i9dQZF1DX8f9HUGHP1Xg"
    BEST_RAP_SONGS_OF_1992_MEGA = "58hTheejkKhupFRkF0yHCE"

    BEST_RAP_SONGS_OF_1993 = "37i9dQZF1DWSJEMFaWwYG8"
    BEST_RAP_SONGS_OF_1993_MEGA = "3fCuYSVaGHXqBqIx4IzHah"

    BEST_RAP_SONGS_OF_1994 = "37i9dQZF1DX16FYsDCnfL9"
    BEST_RAP_SONGS_OF_1994_MEGA = "1DXeuNoVnPs5STDQgTST9Q"

    BEST_RAP_SONGS_OF_1995 = "37i9dQZF1DWXV0QpJtpYUl"
    BEST_RAP_SONGS_OF_1995_MEGA = "3hpYYWbfzvCTGQhToYPB4j"

    BEST_RAP_SONGS_OF_1996 = "37i9dQZF1DWZBTZq94G1cV"
    BEST_RAP_SONGS_OF_1996_MEGA = "0ElOVAxG7DSBN15EWK90EA"

    BEST_RAP_SONGS_OF_1997 = "37i9dQZF1DX9KhfaCWRHKQ"
    BEST_RAP_SONGS_OF_1997_MEGA = "7mpU73hcZMqC941R07Kwut"

    BEST_RAP_SONGS_OF_1998 = "37i9dQZF1DX7VcaGgd2NjX"
    BEST_RAP_SONGS_OF_1998_MEGA = "2hb7d9rRcBYh3pUmGVJbAY"

    BEST_RAP_SONGS_OF_1999 = "37i9dQZF1DXaTwkarURqqL"
    BEST_RAP_SONGS_OF_1999_MEGA = "5rWzFvR1wUvbupFn6AN6ZE"

    BEST_RAP_SONGS_OF_2000 = "37i9dQZF1DX38t16fuNXJJ"
    BEST_RAP_SONGS_OF_2000_MEGA = "3CgpseJD26W7JHV7Lz55qg"

    BEST_RAP_SONGS_OF_2001 = "37i9dQZF1DX84LhXlEFtT2"
    BEST_RAP_SONGS_OF_2001_MEGA = "1LQDVMhpMnuOHQ64sIzRoQ"

    BEST_RAP_SONGS_OF_2002 = "37i9dQZF1DX3CFCcirpUyC"
    BEST_RAP_SONGS_OF_2002_MEGA = "3pGDwVOSNWbKC636MrXf7E"

    BEST_RAP_SONGS_OF_2003 = "37i9dQZF1DWXODx4qTnKLo"
    BEST_RAP_SONGS_OF_2003_MEGA = "2B2eOKpYPLoBcsGBIxhPuw"

    BEST_RAP_SONGS_OF_2004 = "37i9dQZF1DWV5Yt9QQ1nyU"
    BEST_RAP_SONGS_OF_2004_MEGA = "4uQSW7p1nZ9fuCNiyxf9fP"

    BEST_RAP_SONGS_OF_2005 = "37i9dQZF1DWZVgxgOsMmRi"
    BEST_RAP_SONGS_OF_2005_MEGA = "6FtNb3tMLQ8R8F8oAA9NBa"

    BEST_RAP_SONGS_OF_2006 = "37i9dQZF1DX03tzurzdSBW"
    BEST_RAP_SONGS_OF_2006_MEGA = "2uip4xJ1JgdhkuPGPvfYRc"

    BEST_RAP_SONGS_OF_2007 = "37i9dQZF1DX6agKta0oCzd"
    BEST_RAP_SONGS_OF_2007_MEGA = "37INW1245Q8PxlmqlQy941"

    BEST_RAP_SONGS_OF_2008 = "37i9dQZF1DXccZmQhlQC8y"
    BEST_RAP_SONGS_OF_2008_MEGA = "0VGP9M79jIaAYbWKNGzhTD"

    BEST_RAP_SONGS_OF_2009 = "37i9dQZF1DWX2EeJTd5hhM"
    BEST_RAP_SONGS_OF_2009_MEGA = "3vWmF9FiwKDYRnjcVwLfgc"

    BEST_RAP_SONGS_OF_2010 = "37i9dQZF1DWSMW5YBCZisa"
    BEST_RAP_SONGS_OF_2010_MEGA = "11fdoXcUovaXzL4PQ04lQs"

    BEST_RAP_SONGS_OF_2011 = "37i9dQZF1DX3D1xvN8LjbH"
    BEST_RAP_SONGS_OF_2011_MEGA = "4MX3saz9LRD3Gk8aPtrQT5"

    BEST_RAP_SONGS_OF_2012 = "37i9dQZF1DX92DV8EP7bwz"
    BEST_RAP_SONGS_OF_2012_MEGA = "37kREJLw9TliUrfwnoBom1"

    BEST_RAP_SONGS_OF_2013 = "37i9dQZF1DWSWuGRBgXzLE"
    BEST_RAP_SONGS_OF_2013_MEGA = "5mAys0gus3gr23nLyHdCN4"

    BEST_RAP_SONGS_OF_2014 = "37i9dQZF1DWTOgTfzyNaei"
    BEST_RAP_SONGS_OF_2014_MEGA = "3z8r23qZKbUAGkMAMnBHtL"

    BEST_RAP_SONGS_OF_2015 = "37i9dQZF1DXcqWbpeXswkc"
    BEST_RAP_SONGS_OF_2015_MEGA = "1nmUlnGshx4pAgvxp6pVcs"

    BEST_RAP_SONGS_OF_2016 = "37i9dQZF1DWZSCnPqfx5XX"
    BEST_RAP_SONGS_OF_2016_MEGA = "4jGKIf3YJLWeokVyeRv7n6"

    BEST_RAP_SONGS_OF_2017 = "37i9dQZF1DWUUeLChAs7Px"
    BEST_RAP_SONGS_OF_2017_MEGA = "0wPGiw2nEzSI1gVr17vI5E"

    BEST_RAP_SONGS_OF_2018 = "37i9dQZF1DWVXS1PI6Zs44"
    BEST_RAP_SONGS_OF_2018_MEGA = "5as851TKhE13u52WJ5zDmn"

    BEST_RAP_SONGS_OF_2019 = "37i9dQZF1DWZiyat8YCzeB"
    BEST_RAP_SONGS_OF_2019_MEGA = "3prfvRfdhZjrYcph4GgxIH"

    BEST_RAP_SONGS_OF_1990S_COLLECTION = "7bebOMb0peYgIn7Myzn8uj"
    BEST_RAP_SONGS_OF_2000S_COLLECTION = "15CgFL5KT3ZzIsEiZhA0mf"
    BEST_RAP_SONGS_OF_2010S_COLLECTION = "3AVvdTWbtzZM1OQeg4Q6T3"
    BEST_RAP_SONGS_OF_2000_TO_2019_COLLECTION = "2IMPUiYeimMDAOPJ1rkstv"

    # DEUTSCHRAP

    DEUTSCHRAP_1998 = "37i9dQZF1DWWgpcmreqzjG"
    DEUTSCHRAP_1998_MEGA = "5RKU4dqySBdJ4WQjnjcQQj"

    DEUTSCHRAP_1999 = "37i9dQZF1DX4Qj1AcHpQPB"
    DEUTSCHRAP_1999_MEGA = "2nsUlJAxav6AxYcDCKaAcj"

    DEUTSCHRAP_2000 = "37i9dQZF1DXaICQjRilvfY"
    DEUTSCHRAP_2000_MEGA = "3ckCUN55JRI9qOoTHhIpFz"

    DEUTSCHRAP_2001 = "37i9dQZF1DWTvcE6nNtMfB"
    DEUTSCHRAP_2001_MEGA = "5YNzUCRGtoFEShFYlbJs0t"

    DEUTSCHRAP_2002 = "37i9dQZF1DWZ3ZWrdOhV0r"
    DEUTSCHRAP_2002_MEGA = "56Aaas0vyGUqjbQrm7JED7"

    DEUTSCHRAP_2003 = "37i9dQZF1DX5jhxt8O3Avg"
    DEUTSCHRAP_2003_MEGA = "1aeKfXd1MAqsMqB0gNBJe5"

    DEUTSCHRAP_2004 = "37i9dQZF1DWUTKtvDvSZKt"
    DEUTSCHRAP_2004_MEGA = "24q35pqjaY4jhydOccMQy9"

    DEUTSCHRAP_2005 = "37i9dQZF1DX2uR7gB1l1vW"
    DEUTSCHRAP_2005_MEGA = "0Ab11F13a2VB1I6vksWIaa"

    DEUTSCHRAP_2006 = "37i9dQZF1DWXyzwMdgpsAr"
    DEUTSCHRAP_2006_MEGA = "4Dvw6ocEyyITpEANdD594f"

    DEUTSCHRAP_2007 = "37i9dQZF1DXdkiDfFquiVf"
    DEUTSCHRAP_2007_MEGA = "3cFyoTmbKNZVIwLMHUDdhh"

    DEUTSCHRAP_2008 = "37i9dQZF1DWT12DC8fEbej"
    DEUTSCHRAP_2008_MEGA = "12YjvfNMm2pB3oWGt4HBUD"

    DEUTSCHRAP_2009 = "37i9dQZF1DWSQ8cBZBrAqD"
    DEUTSCHRAP_2009_MEGA = "5l1E0aDryPEiA815FLLWRX"

    DEUTSCHRAP_2010 = "37i9dQZF1DX5ro7Zloe2Ob"
    DEUTSCHRAP_2010_MEGA = "6f6PxTgpNqTvSUBsdxlcFD"

    DEUTSCHRAP_2011 = "37i9dQZF1DWVYt8k31ngSV"
    DEUTSCHRAP_2011_MEGA = "7GUtYWcOiBAKaJgo46ClAn"

    DEUTSCHRAP_2012 = "37i9dQZF1DWTI7TfLiJIrq"
    DEUTSCHRAP_2012_MEGA = "4fpqm2QcJ2cZMrxMRz7LAu"

    DEUTSCHRAP_2013 = "37i9dQZF1DWWLsZ6NFYClg"
    DEUTSCHRAP_2013_MEGA = "3N3JhpCfzstQ0cdGOS8m6v"

    DEUTSCHRAP_2014 = "37i9dQZF1DX2YZKWiqxpBl"
    DEUTSCHRAP_2014_MEGA = "02eft6FBjzoXYLMpdVaov0"

    DEUTSCHRAP_2015 = "37i9dQZF1DWVMNqadsMARW"
    DEUTSCHRAP_2015_MEGA = "0d2IDw0DaHdKscew7iYLI3"

    DEUTSCHRAP_2016 = "37i9dQZF1DX8u3hjJj9aOL"
    DEUTSCHRAP_2016_MEGA = "62MMbzT0L9fMo1IHN0fihy"

    DEUTSCHRAP_2017 = "37i9dQZF1DX7x1OfwMMCE6"
    DEUTSCHRAP_2017_MEGA = "5JQxZDks238yKzkfFdliqU"

    DEUTSCHRAP_2018 = "37i9dQZF1DWYB5PBp2PNK1"
    DEUTSCHRAP_2018_MEGA = "0fdnp9XXpGbDLToC7EFQdb"

    DEUTSCHRAP_2019 = "37i9dQZF1DX7mZLuTMCz3p"
    DEUTSCHRAP_2019_MEGA = "1ojY02gF45rJd2KN1B3YBR"

    DEUTSCHRAP_2000S_COLLECTION = "7aTq9MRt4KpftJYmwjS3tV"
    DEUTSCHRAP_2010S_COLLECTION = "5MtK0mZAXaE8zjQuFgKODA"
    DEUTSCHRAP_2000_TO_2019_COLLECTION = "2XQIFCSQD56YHnb6wQ35JP"

    # TIMECAPSULE BACKUP
    THROWBACK_PARTY = "37i9dQZF1DX7F6T2n2fegs"
    THROWBACK_PARTY_MEGA = "79vRQZbrQN9rReANkwL2kH"

    # BACKUP PLAYLISTS
    ROCK_COVERS = "37i9dQZF1DX2S9rTKTX6JP"
    ROCK_COVERS_MEGA = "2v2czgLB3PLPs2QfEBDfMS"

    ROCK_N_RUN = "37i9dQZF1DWXx3Txis2L4x"
    ROCK_N_RUN_MEGA = "6AOoWSiDCscacGdzlIxiqb"

    DEUTSCHRAP_PARTY_HITS = "1Npmu5t4HFkrAYYd0KCSzl"
    DEUTSCHRAP_PARTY_HITS_MEGA = "0OOmkFwAyDyudNur4fyX57"

    LOCKED_IN = "37i9dQZF1DWTl4y3vgJOXW"
    LOCKED_IN_MEGA = "4Bur77687vhMtVRJkJ4IOx"

    POPLAND_2000_2009 = "37i9dQZF1DWVbwsNVGejNr"
    POPLAND_2000_2009_MEGA = "55IRdueM8uLZP3rX3MySs9"

    POPLAND_2010_2019 = "37i9dQZF1DX2cNqJ4LgCMf"
    POPLAND_2010_2019_MEGA = "2DxnZlFq33QDiGOWDR4blN"

    POPLAND = "37i9dQZF1DXbKGrOUA30KN"
    POPLAND_MEGA = "595OoHgRDp0x6pdsRnSngU"

    POPLAND_ALL = "5yofyUqkiSE5RFmpni7N9e"
