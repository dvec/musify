import json
import math
import re
from datetime import datetime, timedelta
from multiprocessing.pool import ThreadPool
from time import sleep
from typing import Optional

import requests
from bs4 import BeautifulSoup as BS

try:
    from musify.config import SP_CLIENT, CACHE_PATH
    from musify.lib import create_id_set, read_playlist_chunks, chunks
    from musify.playlists import Playlist
except Exception:
    from config import SP_CLIENT, CACHE_PATH
    from lib import create_id_set, read_playlist_chunks, chunks
    from playlists import Playlist

MATCH_LIVE = re.compile("\(live?.+$")


def clean_song_title(title: str):
    new_title = title.lower().replace(" - ", " ").replace("feat.", "").replace(" & ", " ").replace("  ", " ").strip()

    new_title = re.sub(MATCH_LIVE, '', new_title)
    return new_title


def spotify_search(song: str):
    search_results = SP_CLIENT.search(q=clean_song_title(song), type="track", limit=1)["tracks"]["items"]

    return song, search_results


class RadioStation:
    def __init__(self, station_id: str, spotify_playlist_id: str, sub_cache_dir: Optional[str] = None):
        self.station_id = station_id.lower()
        self.playlist_id = spotify_playlist_id

        cache_dir = CACHE_PATH.joinpath("radio_station")

        if sub_cache_dir:
            cache_dir = cache_dir.joinpath(sub_cache_dir)

        self.cache_dir = cache_dir.joinpath(station_id)
        self.cache_dir.mkdir(exist_ok=True, parents=True)

        self.not_found_file = self.cache_dir.joinpath(f"{station_id}_not_found.json")
        if not self.not_found_file.exists():
            with self.not_found_file.open("w") as fw:
                json.dump([], fw)

        self.song_map_file = self.cache_dir.joinpath(f"{station_id}_song_map.json")
        if not self.song_map_file.exists():
            with self.song_map_file.open("w") as fw:
                json.dump({}, fw)

        self.source_url: str = None

    def last_played(self, *args, **kwargs) -> set[str]:
        raise NotImplementedError


class RadioBoxStation(RadioStation):
    def __init__(self, *args, country="ch", **kwargs):
        super().__init__(*args, sub_cache_dir="radio_box", **kwargs)

        self.country = country.lower()
        self.source_url = f"https://onlineradiobox.com/{self.country}/{self.station_id}/playlist/{'{}'}?ajax=1&tz=0"

    def last_played(self, *args, **kwargs):
        selector = ".tablelist-schedule > tbody > tr > td > a"

        songs = set()
        for i in range(7):
            soup = BS(requests.get(self.source_url.format(i)).json()["data"], "lxml")

            for x in soup.select(selector):
                song = x.text.lower()

                songs.add(song)

        return songs


def energy_request_history(url: str) -> set[str]:
    for count in range(5):
        if count > 0:
            sleep(count ** 2)

        try:
            response = requests.get(url)
            data = response.json()

            break
        except (requests.exceptions.ConnectTimeout, requests.exceptions.ConnectionError) as ex:
            continue
        except Exception as ex:
            print("error:", url, ex)
            return set()
    else:
        print("error:", f"timeout {url}")
        return set()

    songs = set()
    for record in data:
        if "artist" not in record:
            continue

        songs.add(f"{record['artist']} - {record['title']}".lower())

    return songs


class EnergyStation(RadioStation):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, sub_cache_dir="energy", **kwargs)

        self.source_url = f"https://energy.ch/api/channels/{self.station_id}/playouts?createdAtMax={'{}'}&createdAtMin={'{}'}"

    def last_played(self, *args, last_days=7, **kwargs):
        now = datetime.now()

        overlap_time = timedelta(minutes=5)
        time_window = timedelta(minutes=50)

        upper_bound, lower_bound = now, now - time_window

        songs = set()

        urls = []
        for x in range(math.ceil(
                int(timedelta(days=last_days).total_seconds() / 60) / int(time_window.total_seconds() / 60)
        )):
            created_at_max = (upper_bound + overlap_time).strftime("%Y-%m-%d %H:%M")
            created_at_min = lower_bound.strftime("%Y-%m-%d %H:%M")
            upper_bound, lower_bound = lower_bound, lower_bound - time_window

            urls.append(self.source_url.format(created_at_max, created_at_min))

        with ThreadPool(24) as pool:
            for result in pool.map(energy_request_history, urls, chunksize=10):
                songs = songs.union(result)

        return songs


PLAYLISTS = [
    RadioBoxStation("virginrock", Playlist.VIRGIN_ROCK.value),
    RadioBoxStation("virgin", Playlist.VIRGIN_RADIO.value),
    RadioBoxStation("radioswisspop", Playlist.RADIO_SWISS_POP.value),
    EnergyStation("zuerich", Playlist.ENERGY_ZUERICH.value),
    EnergyStation("atwork", Playlist.ENERGY_AT_WORK.value),
    EnergyStation("top30", Playlist.ENERGY_TOP_30.value),
    EnergyStation("rock", Playlist.ENERGY_ROCK.value),
    EnergyStation("hits", Playlist.ENERGY_HITS.value),
    EnergyStation("00s", Playlist.ENERGY_00S.value),
    EnergyStation("10s", Playlist.ENERGY_10S.value),
    EnergyStation("80s", Playlist.ENERGY_80S.value),
    EnergyStation("90s", Playlist.ENERGY_90S.value),
    EnergyStation("german", Playlist.ENERGY_GERMAN.value),
    EnergyStation("lounge", Playlist.ENERGY_LOUNGE.value),
    EnergyStation("love", Playlist.ENERGY_LOVE.value),
    EnergyStation("metime", Playlist.ENERGY_METIME.value),
    EnergyStation("partyvibes", Playlist.ENERGY_PARTYVIBES.value),
    EnergyStation("pop", Playlist.ENERGY_POP.value),
    EnergyStation("special3", Playlist.ENERGY_SPECIAL3.value),
    EnergyStation("strassenrap", Playlist.ENERGY_STRASSENRAP.value),
    EnergyStation("summervibes", Playlist.ENERGY_SUMMERVIBES.value),
    EnergyStation("urban", Playlist.ENERGY_URBAN.value),
    EnergyStation("workout", Playlist.ENERGY_WORKOUT.value),
    EnergyStation("womxn", Playlist.ENERGY_WOMXN.value),
]


def main(retry_not_found=False):
    for playlist in PLAYLISTS:
        print(playlist.station_id)

        with playlist.song_map_file.open() as fr:
            song_map = {k.lower(): v for k, v in json.load(fr).items()}

        with playlist.not_found_file.open() as fr:
            not_found_set = set([x.lower() for x in json.load(fr)])

        songs = playlist.last_played()
        new_songs = songs.difference(set(song_map))

        if retry_not_found:
            new_songs = new_songs.union(not_found_set)

        if len(new_songs) == 0:
            print("Nothing new!")
            continue

        print("Searching", len(new_songs), "songs")
        song_ids = set()
        with ThreadPool(8) as pool:
            for song, search_results in pool.map(spotify_search, new_songs):
                if not search_results:
                    print(f"Not found: {song}")
                    not_found_set.add(song)
                    continue

                if song in not_found_set:
                    not_found_set.remove(song)

                song_map[song] = search_results[0]["id"]
                song_ids.add(search_results[0]["id"])

        if len(song_ids) > 0:
            dest_ids = create_id_set([x for x in read_playlist_chunks(playlist.playlist_id)])

            new_ids = song_ids.difference(dest_ids)

            for chunk in chunks(new_ids, 50):
                SP_CLIENT.playlist_add_items(playlist.playlist_id, chunk, position=0)

            print("Added", len(new_ids), "songs")

        with playlist.song_map_file.open("w") as fw:
            json.dump(song_map, fw)

        with playlist.not_found_file.open("w") as fw:
            json.dump(list(not_found_set), fw)


if __name__ == '__main__':
    main()
