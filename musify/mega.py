import sys

try:
    from musify.config import SP_CLIENT
    from musify.fvckboi import auto_match_fvckboi, up_to_date_fvckboi
    from musify.lib import create_id_set, read_playlist_chunks, chunks, flat_list
    from musify.playlists import Playlist
except Exception:
    from config import SP_CLIENT
    from fvckboi import auto_match_fvckboi, up_to_date_fvckboi
    from lib import create_id_set, read_playlist_chunks, chunks, flat_list
    from playlists import Playlist

AMIGA_FLOW = [
    (Playlist.RAP_CAVIAR, Playlist.RAP_CAVIAR_MEGA),
    (Playlist.HIP_HOP_MIXTAPE, Playlist.HIP_HOP_MIXTAPE_MEGA),
    (Playlist.HIP_HOP_WORLD_STARS, Playlist.HIP_HOP_WORLD_STARS_MEGA),

    (
        Playlist.RAP_CAVIAR_MEGA,
        Playlist.HIP_HOP_MIXTAPE_MEGA,
        Playlist.AMIGA
    ),
]

DEGA_FLOW = [
    (Playlist.MODUS_MIO, Playlist.MODUS_MIO_MEGA),
    (Playlist.DEUTSCHRAP_ROYAL, Playlist.DEUTSCHRAP_ROYAL_MEGA),
    (Playlist.DEUTSCHRAP_BRANDNEU, Playlist.DEUTSCHRAP_BRANDNEU_MEGA),
    (Playlist.SHISHA_FLOW, Playlist.SHISHA_FLOW_MEGA),
    (Playlist.DEUTSCHTRAP, Playlist.DEUTSCHTRAP_MEGA),
    (Playlist.RAP_IN_DEEP, Playlist.RAP_IN_DEEP_MEGA),
    (Playlist.DEUTSCHRAP_UNTERGRUND, Playlist.DEUTSCHRAP_UNTERGRUND_MEGA),

    (
        Playlist.MODUS_MIO_MEGA,
        Playlist.DEUTSCHRAP_ROYAL_MEGA,
        Playlist.DEUTSCHRAP_BRANDNEU_MEGA,
        Playlist.DEUTSCHTRAP_MEGA,
        Playlist.RAP_IN_DEEP_MEGA,
        Playlist.DEUTSCHRAP_UNTERGRUND_MEGA,
        Playlist.DEUTSCHRAP_MEGA
    ),

    (Playlist.DEUTSCHRAP_PARTY_HITS, Playlist.DEUTSCHRAP_PARTY_HITS_MEGA),
]

TOGA_FLOW = [
    (Playlist.TODAYS_TOP_HITS, Playlist.TODAYS_TOP_HITS_MEGA),
    (Playlist.TOP_HITS_2000_2020, Playlist.TOP_HITS_2000_2020_MEGA),
    (Playlist.TOP_HITS_2000_2020_2, Playlist.TOP_HITS_2000_2020_2_MEGA),
    (Playlist.TOP_2000_2020_HITS, Playlist.TOP_2000_2020_HITS_MEGA),
    (Playlist.SOFT_POP_HITS, Playlist.SOFT_POP_HITS_MEGA),
    (Playlist.JUST_HITS, Playlist.JUST_HITS_MEGA),

    (
        Playlist.TODAYS_TOP_HITS_MEGA,
        Playlist.TOP_HITS_2000_2020_MEGA,
        Playlist.TOP_HITS_2000_2020_2_MEGA,
        Playlist.TOP_2000_2020_HITS_MEGA,
        Playlist.SOFT_POP_HITS_MEGA,
        Playlist.JUST_HITS_MEGA,
        Playlist.TOP_HITS_MEGA
    ),
]

METALIN_FLOW = [
    (Playlist.ADRENALIN_WORKOUT_PLUS, Playlist.ADRENALIN_WORKOUT_PLUS_MEGA),
    (Playlist.ADRENALIN_WORKOUT, Playlist.ADRENALIN_WORKOUT_MEGA),
    (Playlist.ADRENALIN_WORKOUT_2, Playlist.ADRENALIN_WORKOUT_2_MEGA),
    (Playlist.WORKOUT_METAL, Playlist.WORKOUT_METAL_MEGA),
    (Playlist.HEAVY_WORKOUT_METAL_HARD_ROCK, Playlist.HEAVY_WORKOUT_METAL_HARD_ROCK_MEGA),
    (Playlist.WORKOUT_ROCK, Playlist.WORKOUT_ROCK_MEGA),
    (Playlist.WORKOUT_ROCK_METAL, Playlist.WORKOUT_ROCK_METAL_MEGA),
]

GENERIC_FLOW = [
    (Playlist.EVENING_COMMUTE, Playlist.EVENING_COMMUTE_MEGA),
    (Playlist.HAPPY_HITS, Playlist.HAPPY_HITS_MEGA),
    (Playlist.GET_HOME_HAPPY, Playlist.GET_HOME_HAPPY_MEGA),
    (Playlist.SONGS_SING_SHOWER, Playlist.SONGS_SING_SHOWER_MEGA),
    (Playlist.TOP_50_GLOBAL, Playlist.TOP_50_GLOBAL_MEGA),
    (Playlist.SONGS_SING_CAR, Playlist.SONGS_SING_CAR_MEGA),
    (Playlist.MORNING_COMMUTE, Playlist.MORNING_COMMUTE_MEGA),

    (
        Playlist.EVENING_COMMUTE_MEGA,
        Playlist.HAPPY_HITS_MEGA,
        Playlist.GET_HOME_HAPPY_MEGA,
        Playlist.SONGS_SING_SHOWER_MEGA,
        Playlist.SONGS_SING_CAR_MEGA,
        Playlist.MORNING_COMMUTE_MEGA,
        Playlist.GENERIC_MEGA
    ),
]

MISC_FLOW = [
    (Playlist.CHILL_HITS, Playlist.CHILL_HITS_MEGA),
    # (Playlist.GYM_MOTIVATION_2021, Playlist.GYM_MOTIVATION_2021_MEGA),
    # (Playlist.GYM_ROCK_MUSIC, Playlist.GYM_ROCK_MUSIC_MEGA),
    # (Playlist.ESL_ONE_CSGO_OFFICIAL_SOUNDTRACK, Playlist.ESL_ONE_CSGO_OFFICIAL_SOUNDTRACK_MEGA),
    # (Playlist.ESL_PRO_LEAGUE_OFFICIAL_SOUNDTRACK, Playlist.ESL_PRO_LEAGUE_OFFICIAL_SOUNDTRACK_MEGA),
    # (Playlist.GYM_MOTIVATION_WORKOUT_MUSIC, Playlist.GYM_MOTIVATION_WORKOUT_MUSIC_MEGA),
    (Playlist.HAGEN_CLASSICS, Playlist.HAGEN_CLASSICS_MEGA),
    (Playlist.STEGI_DER_SHIT, Playlist.STEGI_DER_SHIT_MEGA),
]

FVCKBOI_FLOW = [
    (Playlist.FVCKBOI, Playlist.FVCKBOI_MEGA),
]

COLLECT_FLOW = [
    (Playlist.TOP_SONGS_GLOBAL, Playlist.TOP_SONGS_GLOBAL_MEGA),
    (Playlist.POP_RISING, Playlist.POP_RISING_MEGA),
    (Playlist.SINGLED_OUT, Playlist.SINGLED_OUT_MEGA),
    (Playlist.FRESH_CHILL, Playlist.FRESH_CHILL_MEGA),
    (Playlist.SONGS_SCREAM_CAR, Playlist.SONGS_SCREAM_CAR_MEGA),
    (Playlist.MEGA_HIT_MIX, Playlist.MEGA_HIT_MIX_MEGA),
    (Playlist.HIT_REWIND, Playlist.HIT_REWIND_MEGA),
    (Playlist.ROCK_THIS, Playlist.ROCK_THIS_MEGA),
]

FRESH_FINDS_FLOW = [
    (Playlist.FRESH_FINDS, Playlist.FRESH_FINDS_MEGA),
    (Playlist.FRESH_FINDS_POP, Playlist.FRESH_FINDS_POP_MEGA),
    (Playlist.FRESH_FINDS_COUNTRY, Playlist.FRESH_FINDS_COUNTRY_MEGA),
    (Playlist.FRESH_FINDS_SG_MY, Playlist.FRESH_FINDS_SG_MY_MEGA),
    (Playlist.FRESH_FINDS_ROCK, Playlist.FRESH_FINDS_ROCK_MEGA),
    (Playlist.FRESH_FINDS_EXPERIMENTAL, Playlist.FRESH_FINDS_EXPERIMENTAL_MEGA),
    (Playlist.FRESH_FINDS_INDIE, Playlist.FRESH_FINDS_INDIE_MEGA),
    (Playlist.FRESH_FINDS_BASEMENT, Playlist.FRESH_FINDS_BASEMENT_MEGA),
    (Playlist.FRESH_FINDS_WAVE, Playlist.FRESH_FINDS_WAVE_MEGA),
    (Playlist.FRESH_FINDS_HIP_HOP, Playlist.FRESH_FINDS_HIP_HOP_MEGA),
]

ROCK_FLOW = [
    (
        Playlist.ROCK_ANTHEMS_60,
        Playlist.ROCK_ANTHEMS_70,
        Playlist.ROCK_ANTHEMS_80,
        Playlist.ROCK_ANTHEMS_90,
        Playlist.ROCK_ANTHEMS_00,
        Playlist.ROCK_ANTHEMS_10,
        Playlist.ROCK_ANTHEMS_MEGA
    ),

    (Playlist.ROCK_CLASSICS, Playlist.ROCK_CLASSICS_MEGA),
    (Playlist.ROCK_ROTATION, Playlist.ROCK_ROTATION_MEGA),
    (Playlist.ROCK_PARTY, Playlist.ROCK_PARTY_MEGA),
    (Playlist.ROCKHYMNEN, Playlist.ROCKHYMNEN_MEGA),
    (Playlist.ROCK_SOLID, Playlist.ROCK_SOLID_MEGA),

    (
        Playlist.ROCK_ANTHEMS_MEGA,
        Playlist.ROCK_CLASSICS_MEGA,
        Playlist.ROCK_ROTATION_MEGA,
        Playlist.ROCK_PARTY_MEGA,
        Playlist.ROCKHYMNEN_MEGA,
        Playlist.ROCK_SOLID_MEGA,
        Playlist.ROCK_MIX_MEGA
    ),

    (Playlist.ROCK_COVERS, Playlist.ROCK_COVERS_MEGA),
    (Playlist.ROCK_N_RUN, Playlist.ROCK_N_RUN_MEGA),
]

ALL_OUT_FLOW = [
    (Playlist.ALL_OUT_60S, Playlist.ALL_OUT_60S_MEGA),
    (Playlist.ALL_OUT_70S, Playlist.ALL_OUT_70S_MEGA),
    (Playlist.ALL_OUT_80S, Playlist.ALL_OUT_80S_MEGA),
    (Playlist.ALL_OUT_90S, Playlist.ALL_OUT_90S_MEGA),
    (Playlist.ALL_OUT_2000S, Playlist.ALL_OUT_2000S_MEGA),
    (Playlist.ALL_OUT_2010S, Playlist.ALL_OUT_2010S_MEGA),
]

_TOP_HITS_OF_1970S = [
    (Playlist.TOP_HITS_OF_1970, Playlist.TOP_HITS_OF_1970_MEGA),
    (Playlist.TOP_HITS_OF_1971, Playlist.TOP_HITS_OF_1971_MEGA),
    (Playlist.TOP_HITS_OF_1972, Playlist.TOP_HITS_OF_1972_MEGA),
    (Playlist.TOP_HITS_OF_1973, Playlist.TOP_HITS_OF_1973_MEGA),
    (Playlist.TOP_HITS_OF_1974, Playlist.TOP_HITS_OF_1974_MEGA),
    (Playlist.TOP_HITS_OF_1975, Playlist.TOP_HITS_OF_1975_MEGA),
    (Playlist.TOP_HITS_OF_1976, Playlist.TOP_HITS_OF_1976_MEGA),
    (Playlist.TOP_HITS_OF_1977, Playlist.TOP_HITS_OF_1977_MEGA),
    (Playlist.TOP_HITS_OF_1978, Playlist.TOP_HITS_OF_1978_MEGA),
    (Playlist.TOP_HITS_OF_1979, Playlist.TOP_HITS_OF_1979_MEGA),
]

_TOP_HITS_OF_1980S = [
    (Playlist.TOP_HITS_OF_1980, Playlist.TOP_HITS_OF_1980_MEGA),
    (Playlist.TOP_HITS_OF_1981, Playlist.TOP_HITS_OF_1981_MEGA),
    (Playlist.TOP_HITS_OF_1982, Playlist.TOP_HITS_OF_1982_MEGA),
    (Playlist.TOP_HITS_OF_1983, Playlist.TOP_HITS_OF_1983_MEGA),
    (Playlist.TOP_HITS_OF_1984, Playlist.TOP_HITS_OF_1984_MEGA),
    (Playlist.TOP_HITS_OF_1985, Playlist.TOP_HITS_OF_1985_MEGA),
    (Playlist.TOP_HITS_OF_1986, Playlist.TOP_HITS_OF_1986_MEGA),
    (Playlist.TOP_HITS_OF_1987, Playlist.TOP_HITS_OF_1987_MEGA),
    (Playlist.TOP_HITS_OF_1988, Playlist.TOP_HITS_OF_1988_MEGA),
    (Playlist.TOP_HITS_OF_1989, Playlist.TOP_HITS_OF_1989_MEGA),
]

_TOP_HITS_OF_1990S = [
    (Playlist.TOP_HITS_OF_1990, Playlist.TOP_HITS_OF_1990_MEGA),
    (Playlist.TOP_HITS_OF_1991, Playlist.TOP_HITS_OF_1991_MEGA),
    (Playlist.TOP_HITS_OF_1992, Playlist.TOP_HITS_OF_1992_MEGA),
    (Playlist.TOP_HITS_OF_1993, Playlist.TOP_HITS_OF_1993_MEGA),
    (Playlist.TOP_HITS_OF_1994, Playlist.TOP_HITS_OF_1994_MEGA),
    (Playlist.TOP_HITS_OF_1995, Playlist.TOP_HITS_OF_1995_MEGA),
    (Playlist.TOP_HITS_OF_1996, Playlist.TOP_HITS_OF_1996_MEGA),
    (Playlist.TOP_HITS_OF_1997, Playlist.TOP_HITS_OF_1997_MEGA),
    (Playlist.TOP_HITS_OF_1998, Playlist.TOP_HITS_OF_1998_MEGA),
    (Playlist.TOP_HITS_OF_1999, Playlist.TOP_HITS_OF_1999_MEGA),
]

_TOP_HITS_OF_2000S = [
    (Playlist.TOP_HITS_OF_2000, Playlist.TOP_HITS_OF_2000_MEGA),
    (Playlist.TOP_HITS_OF_2001, Playlist.TOP_HITS_OF_2001_MEGA),
    (Playlist.TOP_HITS_OF_2002, Playlist.TOP_HITS_OF_2002_MEGA),
    (Playlist.TOP_HITS_OF_2003, Playlist.TOP_HITS_OF_2003_MEGA),
    (Playlist.TOP_HITS_OF_2004, Playlist.TOP_HITS_OF_2004_MEGA),
    (Playlist.TOP_HITS_OF_2005, Playlist.TOP_HITS_OF_2005_MEGA),
    (Playlist.TOP_HITS_OF_2006, Playlist.TOP_HITS_OF_2006_MEGA),
    (Playlist.TOP_HITS_OF_2007, Playlist.TOP_HITS_OF_2007_MEGA),
    (Playlist.TOP_HITS_OF_2008, Playlist.TOP_HITS_OF_2008_MEGA),
    (Playlist.TOP_HITS_OF_2009, Playlist.TOP_HITS_OF_2009_MEGA),
]

_TOP_HITS_OF_2010S = [
    (Playlist.TOP_HITS_OF_2010, Playlist.TOP_HITS_OF_2010_MEGA),
    (Playlist.TOP_HITS_OF_2011, Playlist.TOP_HITS_OF_2011_MEGA),
    (Playlist.TOP_HITS_OF_2012, Playlist.TOP_HITS_OF_2012_MEGA),
    (Playlist.TOP_HITS_OF_2013, Playlist.TOP_HITS_OF_2013_MEGA),
    (Playlist.TOP_HITS_OF_2014, Playlist.TOP_HITS_OF_2014_MEGA),
    (Playlist.TOP_HITS_OF_2015, Playlist.TOP_HITS_OF_2015_MEGA),
    (Playlist.TOP_HITS_OF_2016, Playlist.TOP_HITS_OF_2016_MEGA),
    (Playlist.TOP_HITS_OF_2017, Playlist.TOP_HITS_OF_2017_MEGA),
    (Playlist.TOP_HITS_OF_2018, Playlist.TOP_HITS_OF_2018_MEGA),
    (Playlist.TOP_HITS_OF_2019, Playlist.TOP_HITS_OF_2019_MEGA),
]

TOP_HITS_OF_FLOW = [
    *_TOP_HITS_OF_1970S,
    *_TOP_HITS_OF_1980S,
    *_TOP_HITS_OF_1990S,
    *_TOP_HITS_OF_2000S,
    *_TOP_HITS_OF_2010S,

    (
        *[x[-1] for x in _TOP_HITS_OF_1970S],
        Playlist.TOP_HITS_OF_1970S_COLLECTION,
    ),
    (
        *[x[-1] for x in _TOP_HITS_OF_1980S],
        Playlist.TOP_HITS_OF_1980S_COLLECTION,
    ),
    (
        *[x[-1] for x in _TOP_HITS_OF_1990S],
        Playlist.TOP_HITS_OF_1990S_COLLECTION,
    ),
    (
        *[x[-1] for x in _TOP_HITS_OF_2000S],
        Playlist.TOP_HITS_OF_2000S_COLLECTION,
    ),
    (
        *[x[-1] for x in _TOP_HITS_OF_2010S],
        Playlist.TOP_HITS_OF_2010S_COLLECTION,
    ),
    (
        Playlist.TOP_HITS_OF_2000S_COLLECTION,
        Playlist.TOP_HITS_OF_2010S_COLLECTION,

        Playlist.TOP_HITS_OF_2000_TO_2019_COLLECTION,
    ),
]

_TOP_HITS_DEUTSCHLAND_1980S = [
    (Playlist.TOP_HITS_DEUTSCHLAND_1980, Playlist.TOP_HITS_DEUTSCHLAND_1980_MEGA),
    (Playlist.TOP_HITS_DEUTSCHLAND_1981, Playlist.TOP_HITS_DEUTSCHLAND_1981_MEGA),
    (Playlist.TOP_HITS_DEUTSCHLAND_1982, Playlist.TOP_HITS_DEUTSCHLAND_1982_MEGA),
    (Playlist.TOP_HITS_DEUTSCHLAND_1983, Playlist.TOP_HITS_DEUTSCHLAND_1983_MEGA),
    (Playlist.TOP_HITS_DEUTSCHLAND_1984, Playlist.TOP_HITS_DEUTSCHLAND_1984_MEGA),
    (Playlist.TOP_HITS_DEUTSCHLAND_1985, Playlist.TOP_HITS_DEUTSCHLAND_1985_MEGA),
    (Playlist.TOP_HITS_DEUTSCHLAND_1986, Playlist.TOP_HITS_DEUTSCHLAND_1986_MEGA),
    (Playlist.TOP_HITS_DEUTSCHLAND_1987, Playlist.TOP_HITS_DEUTSCHLAND_1987_MEGA),
    (Playlist.TOP_HITS_DEUTSCHLAND_1988, Playlist.TOP_HITS_DEUTSCHLAND_1988_MEGA),
    (Playlist.TOP_HITS_DEUTSCHLAND_1989, Playlist.TOP_HITS_DEUTSCHLAND_1989_MEGA),
]

_TOP_HITS_DEUTSCHLAND_1990S = [
    (Playlist.TOP_HITS_DEUTSCHLAND_1990, Playlist.TOP_HITS_DEUTSCHLAND_1990_MEGA),
    (Playlist.TOP_HITS_DEUTSCHLAND_1991, Playlist.TOP_HITS_DEUTSCHLAND_1991_MEGA),
    (Playlist.TOP_HITS_DEUTSCHLAND_1992, Playlist.TOP_HITS_DEUTSCHLAND_1992_MEGA),
    (Playlist.TOP_HITS_DEUTSCHLAND_1993, Playlist.TOP_HITS_DEUTSCHLAND_1993_MEGA),
    (Playlist.TOP_HITS_DEUTSCHLAND_1994, Playlist.TOP_HITS_DEUTSCHLAND_1994_MEGA),
    (Playlist.TOP_HITS_DEUTSCHLAND_1995, Playlist.TOP_HITS_DEUTSCHLAND_1995_MEGA),
    (Playlist.TOP_HITS_DEUTSCHLAND_1996, Playlist.TOP_HITS_DEUTSCHLAND_1996_MEGA),
    (Playlist.TOP_HITS_DEUTSCHLAND_1997, Playlist.TOP_HITS_DEUTSCHLAND_1997_MEGA),
    (Playlist.TOP_HITS_DEUTSCHLAND_1998, Playlist.TOP_HITS_DEUTSCHLAND_1998_MEGA),
    (Playlist.TOP_HITS_DEUTSCHLAND_1999, Playlist.TOP_HITS_DEUTSCHLAND_1999_MEGA),
]

_TOP_HITS_DEUTSCHLAND_2000S = [
    (Playlist.TOP_HITS_DEUTSCHLAND_2000, Playlist.TOP_HITS_DEUTSCHLAND_2000_MEGA),
    (Playlist.TOP_HITS_DEUTSCHLAND_2001, Playlist.TOP_HITS_DEUTSCHLAND_2001_MEGA),
    (Playlist.TOP_HITS_DEUTSCHLAND_2002, Playlist.TOP_HITS_DEUTSCHLAND_2002_MEGA),
    (Playlist.TOP_HITS_DEUTSCHLAND_2003, Playlist.TOP_HITS_DEUTSCHLAND_2003_MEGA),
    (Playlist.TOP_HITS_DEUTSCHLAND_2004, Playlist.TOP_HITS_DEUTSCHLAND_2004_MEGA),
    (Playlist.TOP_HITS_DEUTSCHLAND_2005, Playlist.TOP_HITS_DEUTSCHLAND_2005_MEGA),
    (Playlist.TOP_HITS_DEUTSCHLAND_2006, Playlist.TOP_HITS_DEUTSCHLAND_2006_MEGA),
    (Playlist.TOP_HITS_DEUTSCHLAND_2007, Playlist.TOP_HITS_DEUTSCHLAND_2007_MEGA),
    (Playlist.TOP_HITS_DEUTSCHLAND_2008, Playlist.TOP_HITS_DEUTSCHLAND_2008_MEGA),
    (Playlist.TOP_HITS_DEUTSCHLAND_2009, Playlist.TOP_HITS_DEUTSCHLAND_2009_MEGA),
]

_TOP_HITS_DEUTSCHLAND_2010S = [
    (Playlist.TOP_HITS_DEUTSCHLAND_2010, Playlist.TOP_HITS_DEUTSCHLAND_2010_MEGA),
    (Playlist.TOP_HITS_DEUTSCHLAND_2011, Playlist.TOP_HITS_DEUTSCHLAND_2011_MEGA),
    (Playlist.TOP_HITS_DEUTSCHLAND_2012, Playlist.TOP_HITS_DEUTSCHLAND_2012_MEGA),
    (Playlist.TOP_HITS_DEUTSCHLAND_2013, Playlist.TOP_HITS_DEUTSCHLAND_2013_MEGA),
    (Playlist.TOP_HITS_DEUTSCHLAND_2014, Playlist.TOP_HITS_DEUTSCHLAND_2014_MEGA),
    (Playlist.TOP_HITS_DEUTSCHLAND_2015, Playlist.TOP_HITS_DEUTSCHLAND_2015_MEGA),
    (Playlist.TOP_HITS_DEUTSCHLAND_2016, Playlist.TOP_HITS_DEUTSCHLAND_2016_MEGA),
    (Playlist.TOP_HITS_DEUTSCHLAND_2017, Playlist.TOP_HITS_DEUTSCHLAND_2017_MEGA),
    (Playlist.TOP_HITS_DEUTSCHLAND_2018, Playlist.TOP_HITS_DEUTSCHLAND_2018_MEGA),
    (Playlist.TOP_HITS_DEUTSCHLAND_2019, Playlist.TOP_HITS_DEUTSCHLAND_2019_MEGA),
]

TOP_HITS_DEUTSCHLAND_FLOW = [
    *_TOP_HITS_DEUTSCHLAND_1980S,
    *_TOP_HITS_DEUTSCHLAND_1990S,
    *_TOP_HITS_DEUTSCHLAND_2000S,
    *_TOP_HITS_DEUTSCHLAND_2010S,

    (
        *[x[-1] for x in _TOP_HITS_DEUTSCHLAND_1980S],
        Playlist.TOP_HITS_DEUTSCHLAND_1980S_COLLECTION,
    ),
    (
        *[x[-1] for x in _TOP_HITS_DEUTSCHLAND_1990S],
        Playlist.TOP_HITS_DEUTSCHLAND_1990S_COLLECTION,
    ),
    (
        *[x[-1] for x in _TOP_HITS_DEUTSCHLAND_2000S],
        Playlist.TOP_HITS_DEUTSCHLAND_2000S_COLLECTION,
    ),
    (
        *[x[-1] for x in _TOP_HITS_DEUTSCHLAND_2010S],
        Playlist.TOP_HITS_DEUTSCHLAND_2010S_COLLECTION,
    ),
    (
        Playlist.TOP_HITS_DEUTSCHLAND_2000S_COLLECTION,
        Playlist.TOP_HITS_DEUTSCHLAND_2010S_COLLECTION,

        Playlist.TOP_HITS_DEUTSCHLAND_2000_TO_2019_COLLECTION,
    ),
]

_BEST_RAP_SONGS_OF_1980S = [
    (Playlist.BEST_RAP_SONGS_OF_1987, Playlist.BEST_RAP_SONGS_OF_1987_MEGA),
    (Playlist.BEST_RAP_SONGS_OF_1988, Playlist.BEST_RAP_SONGS_OF_1988_MEGA),
    (Playlist.BEST_RAP_SONGS_OF_1989, Playlist.BEST_RAP_SONGS_OF_1989_MEGA),
]

_BEST_RAP_SONGS_OF_1990S = [
    (Playlist.BEST_RAP_SONGS_OF_1990, Playlist.BEST_RAP_SONGS_OF_1990_MEGA),
    (Playlist.BEST_RAP_SONGS_OF_1991, Playlist.BEST_RAP_SONGS_OF_1991_MEGA),
    (Playlist.BEST_RAP_SONGS_OF_1992, Playlist.BEST_RAP_SONGS_OF_1992_MEGA),
    (Playlist.BEST_RAP_SONGS_OF_1993, Playlist.BEST_RAP_SONGS_OF_1993_MEGA),
    (Playlist.BEST_RAP_SONGS_OF_1994, Playlist.BEST_RAP_SONGS_OF_1994_MEGA),
    (Playlist.BEST_RAP_SONGS_OF_1995, Playlist.BEST_RAP_SONGS_OF_1995_MEGA),
    (Playlist.BEST_RAP_SONGS_OF_1996, Playlist.BEST_RAP_SONGS_OF_1996_MEGA),
    (Playlist.BEST_RAP_SONGS_OF_1997, Playlist.BEST_RAP_SONGS_OF_1997_MEGA),
    (Playlist.BEST_RAP_SONGS_OF_1998, Playlist.BEST_RAP_SONGS_OF_1998_MEGA),
    (Playlist.BEST_RAP_SONGS_OF_1999, Playlist.BEST_RAP_SONGS_OF_1999_MEGA),
]

_BEST_RAP_SONGS_OF_2000S = [
    (Playlist.BEST_RAP_SONGS_OF_2000, Playlist.BEST_RAP_SONGS_OF_2000_MEGA),
    (Playlist.BEST_RAP_SONGS_OF_2001, Playlist.BEST_RAP_SONGS_OF_2001_MEGA),
    (Playlist.BEST_RAP_SONGS_OF_2002, Playlist.BEST_RAP_SONGS_OF_2002_MEGA),
    (Playlist.BEST_RAP_SONGS_OF_2003, Playlist.BEST_RAP_SONGS_OF_2003_MEGA),
    (Playlist.BEST_RAP_SONGS_OF_2004, Playlist.BEST_RAP_SONGS_OF_2004_MEGA),
    (Playlist.BEST_RAP_SONGS_OF_2005, Playlist.BEST_RAP_SONGS_OF_2005_MEGA),
    (Playlist.BEST_RAP_SONGS_OF_2006, Playlist.BEST_RAP_SONGS_OF_2006_MEGA),
    (Playlist.BEST_RAP_SONGS_OF_2007, Playlist.BEST_RAP_SONGS_OF_2007_MEGA),
    (Playlist.BEST_RAP_SONGS_OF_2008, Playlist.BEST_RAP_SONGS_OF_2008_MEGA),
    (Playlist.BEST_RAP_SONGS_OF_2009, Playlist.BEST_RAP_SONGS_OF_2009_MEGA),
]

_BEST_RAP_SONGS_OF_2010S = [
    (Playlist.BEST_RAP_SONGS_OF_2010, Playlist.BEST_RAP_SONGS_OF_2010_MEGA),
    (Playlist.BEST_RAP_SONGS_OF_2011, Playlist.BEST_RAP_SONGS_OF_2011_MEGA),
    (Playlist.BEST_RAP_SONGS_OF_2012, Playlist.BEST_RAP_SONGS_OF_2012_MEGA),
    (Playlist.BEST_RAP_SONGS_OF_2013, Playlist.BEST_RAP_SONGS_OF_2013_MEGA),
    (Playlist.BEST_RAP_SONGS_OF_2014, Playlist.BEST_RAP_SONGS_OF_2014_MEGA),
    (Playlist.BEST_RAP_SONGS_OF_2015, Playlist.BEST_RAP_SONGS_OF_2015_MEGA),
    (Playlist.BEST_RAP_SONGS_OF_2016, Playlist.BEST_RAP_SONGS_OF_2016_MEGA),
    (Playlist.BEST_RAP_SONGS_OF_2017, Playlist.BEST_RAP_SONGS_OF_2017_MEGA),
    (Playlist.BEST_RAP_SONGS_OF_2018, Playlist.BEST_RAP_SONGS_OF_2018_MEGA),
    (Playlist.BEST_RAP_SONGS_OF_2019, Playlist.BEST_RAP_SONGS_OF_2019_MEGA),
]

BEST_RAP_SONGS_OF_FLOW = [
    *_BEST_RAP_SONGS_OF_1980S,
    *_BEST_RAP_SONGS_OF_1990S,
    *_BEST_RAP_SONGS_OF_2000S,
    *_BEST_RAP_SONGS_OF_2010S,

    (
        *[x[-1] for x in _BEST_RAP_SONGS_OF_1990S],
        Playlist.BEST_RAP_SONGS_OF_1990S_COLLECTION,
    ),
    (
        *[x[-1] for x in _BEST_RAP_SONGS_OF_2000S],
        Playlist.BEST_RAP_SONGS_OF_2000S_COLLECTION,
    ),
    (
        *[x[-1] for x in _BEST_RAP_SONGS_OF_2010S],
        Playlist.BEST_RAP_SONGS_OF_2010S_COLLECTION,
    ),
    (
        Playlist.BEST_RAP_SONGS_OF_2000S_COLLECTION,
        Playlist.BEST_RAP_SONGS_OF_2010S_COLLECTION,

        Playlist.BEST_RAP_SONGS_OF_2000_TO_2019_COLLECTION,
    ),
]

_DEUTSCHRAP_1990S = [
    (Playlist.DEUTSCHRAP_1998, Playlist.DEUTSCHRAP_1998_MEGA),
    (Playlist.DEUTSCHRAP_1999, Playlist.DEUTSCHRAP_1999_MEGA),
]

_DEUTSCHRAP_2000S = [
    (Playlist.DEUTSCHRAP_2000, Playlist.DEUTSCHRAP_2000_MEGA),
    (Playlist.DEUTSCHRAP_2001, Playlist.DEUTSCHRAP_2001_MEGA),
    (Playlist.DEUTSCHRAP_2002, Playlist.DEUTSCHRAP_2002_MEGA),
    (Playlist.DEUTSCHRAP_2003, Playlist.DEUTSCHRAP_2003_MEGA),
    (Playlist.DEUTSCHRAP_2004, Playlist.DEUTSCHRAP_2004_MEGA),
    (Playlist.DEUTSCHRAP_2005, Playlist.DEUTSCHRAP_2005_MEGA),
    (Playlist.DEUTSCHRAP_2006, Playlist.DEUTSCHRAP_2006_MEGA),
    (Playlist.DEUTSCHRAP_2007, Playlist.DEUTSCHRAP_2007_MEGA),
    (Playlist.DEUTSCHRAP_2008, Playlist.DEUTSCHRAP_2008_MEGA),
    (Playlist.DEUTSCHRAP_2009, Playlist.DEUTSCHRAP_2009_MEGA),
]

_DEUTSCHRAP_2010S = [
    (Playlist.DEUTSCHRAP_2010, Playlist.DEUTSCHRAP_2010_MEGA),
    (Playlist.DEUTSCHRAP_2011, Playlist.DEUTSCHRAP_2011_MEGA),
    (Playlist.DEUTSCHRAP_2012, Playlist.DEUTSCHRAP_2012_MEGA),
    (Playlist.DEUTSCHRAP_2013, Playlist.DEUTSCHRAP_2013_MEGA),
    (Playlist.DEUTSCHRAP_2014, Playlist.DEUTSCHRAP_2014_MEGA),
    (Playlist.DEUTSCHRAP_2015, Playlist.DEUTSCHRAP_2015_MEGA),
    (Playlist.DEUTSCHRAP_2016, Playlist.DEUTSCHRAP_2016_MEGA),
    (Playlist.DEUTSCHRAP_2017, Playlist.DEUTSCHRAP_2017_MEGA),
    (Playlist.DEUTSCHRAP_2018, Playlist.DEUTSCHRAP_2018_MEGA),
    (Playlist.DEUTSCHRAP_2019, Playlist.DEUTSCHRAP_2019_MEGA),
]

DEUTSCHRAP_FLOW = [
    *_DEUTSCHRAP_1990S,
    *_DEUTSCHRAP_2000S,
    *_DEUTSCHRAP_2010S,

    (
        *[x[-1] for x in _DEUTSCHRAP_2000S],
        Playlist.DEUTSCHRAP_2000S_COLLECTION,
    ),
    (
        *[x[-1] for x in _DEUTSCHRAP_2010S],
        Playlist.DEUTSCHRAP_2010S_COLLECTION,
    ),
    (
        Playlist.DEUTSCHRAP_2000S_COLLECTION,
        Playlist.DEUTSCHRAP_2010S_COLLECTION,

        Playlist.DEUTSCHRAP_2000_TO_2019_COLLECTION,
    ),
]

TIME_CAPSULE_BACKUP_FLOW = [
    (Playlist.THROWBACK_PARTY, Playlist.THROWBACK_PARTY_MEGA),
]

BACKUP_FLOW = [
    (Playlist.LOCKED_IN, Playlist.LOCKED_IN_MEGA),
]

POPLAND_FLOW = [
    (Playlist.POPLAND_2000_2009, Playlist.POPLAND_2000_2009_MEGA),
    (Playlist.POPLAND_2010_2019, Playlist.POPLAND_2010_2019_MEGA),
    (Playlist.POPLAND, Playlist.POPLAND_MEGA),
    (Playlist.POPLAND_2000_2009_MEGA, Playlist.POPLAND_2010_2019_MEGA, Playlist.POPLAND_MEGA, Playlist.POPLAND_ALL),
]

MY_TIME_FLOW = [
    (
        Playlist.TOP_HITS_OF_1999_MEGA,
        Playlist.TOP_HITS_OF_2000S_COLLECTION,
        Playlist.TOP_HITS_OF_2010_MEGA,
        Playlist.TOP_HITS_OF_2011_MEGA,
        Playlist.ALL_OUT_2000S_MEGA,
        Playlist.ENERGY_00S,

        Playlist.TOP_HITS_OF_MY_TIME_COLLECTION
    ),
]

FLOWS_MAP = {
    "NORMAL": [
        AMIGA_FLOW,
        DEGA_FLOW,
        TOGA_FLOW,
        # METALIN_FLOW,
        GENERIC_FLOW,
        MISC_FLOW,
        COLLECT_FLOW,
        # FRESH_FINDS_FLOW,
        ROCK_FLOW,
        BACKUP_FLOW,
    ],
    "TIME_CAPSULE": [
        ALL_OUT_FLOW,
        TOP_HITS_OF_FLOW,
        TOP_HITS_DEUTSCHLAND_FLOW,
        BEST_RAP_SONGS_OF_FLOW,
        DEUTSCHRAP_FLOW,
        TIME_CAPSULE_BACKUP_FLOW,
        POPLAND_FLOW,
        MY_TIME_FLOW,
    ],
    "FVCKBOI": [
        FVCKBOI_FLOW,
    ],
}


def main(*args, flow_mode="NORMAL", **kwargs):
    flow_mode = flow_mode.upper()
    flow_list = FLOWS_MAP.get(flow_mode, [])

    new_ids_collection = set()

    print(f"FLOW_MODE: {flow_mode} ({len(flow_list)})")
    for flow in flow_list:
        for flow_node in flow:

            src_playlists = []
            for item in flow_node[:-1]:
                try:
                    src_playlists.append(SP_CLIENT.playlist(item.value))
                except Exception as ex:
                    print("error retrieving src playlist", item.value, ex)

            if not src_playlists:
                print("no src_playlists")
                continue

            dest_playlist = SP_CLIENT.playlist(flow_node[-1].value)

            print(f"{' && '.join([x['name'] for x in src_playlists])} -> {dest_playlist['name']}")

            srcs_ids = set()
            for src_playlist in src_playlists:
                srcs_ids.update(create_id_set([x for x in read_playlist_chunks(src_playlist["id"])]))

            dest_ids = create_id_set([x for x in read_playlist_chunks(dest_playlist["id"])])

            new_ids = srcs_ids.difference(dest_ids)

            for chunk in chunks(new_ids, 50):
                SP_CLIENT.playlist_add_items(dest_playlist["id"], chunk, position=0)

            print(f" added {len(new_ids)} track\s to {dest_playlist['name']}")

            new_ids_collection.update(new_ids)

    print(f"Added {len(new_ids_collection)} track\s in TOTAL")


if __name__ == '__main__':
    __args = sys.argv
    if len(__args) > 1:
        __flow_mode = __args[1]
    else:
        __flow_mode = "NORMAL"

    main(flow_mode=__flow_mode)
