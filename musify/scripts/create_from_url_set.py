try:
    from musify.config import SP_CLIENT
except Exception:
    from config import SP_CLIENT


def main(*args, **kwargs):
    urls = {
        ""
    }

    name_id_map = {}

    for url in urls:
        playlist_id = url.split("/")[-1].split("?")[0]
        playlist = SP_CLIENT.playlist(playlist_id)

        name = playlist["name"]
        name_id_map[name] = playlist_id

    for name, playlist_id in sorted(name_id_map.items()):

        clean_name = "_".join(name.upper().split(" "))

        new_playlist = SP_CLIENT.user_playlist_create("dvec.tz", name)

        print(f"{clean_name} = \"{playlist_id}\"")
        print(f"{clean_name}_MEGA = \"{new_playlist['id']}\"")
        print()


if __name__ == '__main__':
    main()
