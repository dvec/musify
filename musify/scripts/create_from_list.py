try:
    from musify.config import SP_CLIENT
except Exception:
    from config import SP_CLIENT


def main(*args, **kwargs):
    playlist_names = [
        "Energy german",
        "Energy 10S",
        "Energy 00S",
        "Energy 90S",
        "Energy 80S",
        "Energy partyvibes",
        "Energy lounge",
        "Energy love",
        "Energy pop",
        "Energy summervibes",
        "Energy strassenrap",
        "Energy special3",
        "Energy workout",
        "Energy urban",
        "Energy metime",
    ]

    for name in sorted(playlist_names):
        clean_name = "_".join(name.upper().split(" "))

        new_playlist = SP_CLIENT.user_playlist_create("dvec.tz", name)

        print(f"{clean_name} = \"{new_playlist['id']}\"")
        print()


if __name__ == '__main__':
    main()
