try:
    from musify.config import SP_CLIENT
    from musify.lib import read_playlist
    from musify.playlists import Playlist
except Exception:
    from ..config import SP_CLIENT
    from ..lib import read_playlist
    from ..playlists import Playlist


def move_new_to_top(playlist_id: str, client=SP_CLIENT):
    """
    !!USE WITH CAUTION!!
    Moves new tracks to the top
    """

    data = read_playlist(playlist_id, client=client)

    oldest_date = None
    break_index = None
    for index, item in enumerate(data):
        date = item["added_at"].split("T")[0]

        if index == 0:
            oldest_date = date
            continue

        if date < oldest_date:
            oldest_date = date
        elif date == oldest_date:
            continue
        else:
            break_index = index
            break

    if break_index:
        print(break_index, (len(data) - break_index), len(data))

        client.playlist_reorder_items(
            playlist_id,
            range_start=break_index,
            range_length=(len(data) - break_index),
            insert_before=0
        )
