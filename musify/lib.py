import functools
import operator
from collections import defaultdict

try:
    from musify.config import SP_CLIENT
except Exception:
    from config import SP_CLIENT


def chunks(lst, n):
    """Yield successive n-sized chunks from lst."""
    lst = list(lst)

    for i in range(0, len(lst), n):
        yield lst[i:i + n]


PLAYLIST_ITEMS_FIELDS = "total,next,href,items.track.id,items.track.name,items.added_at,items.track.artists.id,items.track.artists.name,items.track.type,items.track.album.release_date,items.track.album.release_date_precision"
PLAYLIST_ITEMS_TYPES = ("track",)


def read_playlist_chunks(playlist_id: str, chunk_size: int = 100, client=SP_CLIENT):
    offset = 0

    while True:
        response = client.playlist_items(
            playlist_id,
            offset=offset,
            limit=chunk_size,
            fields=PLAYLIST_ITEMS_FIELDS,
            additional_types=PLAYLIST_ITEMS_TYPES
        )
        offset = offset + len(response["items"])

        for track in response["items"]:
            if track["track"] is None or track["track"]["type"] != "track":
                # track flagged as removed from spotify but still in playlist
                continue

            yield {
                "id": track["track"]["id"],
                "title": track["track"]["name"],
                "added_at": track["added_at"],
                "artists": track["track"]["artists"],
                "type": track["track"]["type"],
                "release_date": track["track"]["album"]["release_date"],
                "release_date_precision": track["track"]["album"]["release_date_precision"],
            }

        if len(response["items"]) == 0:
            break


def read_playlist(playlist_id: str, client=SP_CLIENT):
    return [x for x in read_playlist_chunks(playlist_id, client=client)]


def reverse_playlist(playlist_id: str, client=SP_CLIENT):
    playlist_ids = [x["id"] for x in read_playlist_chunks(playlist_id, client=client)]

    length = len(playlist_ids)
    for i in reversed(range(length)):
        client.playlist_reorder_items(playlist_id, range_start=i, range_length=1, insert_before=length)


def create_id_list(array: list) -> list:
    return [x["id"] for x in array if x["id"] is not None]


def create_id_set(array: list) -> set:
    return set(x["id"] for x in array if x["id"] is not None)


def create_id_map(array: list) -> dict:
    return dict((x["id"], x) for x in array if x["id"] is not None)


def flat_list(nested_list) -> list:
    return functools.reduce(operator.iconcat, nested_list, [])


def unique_track_str(track: dict) -> str:
    """
    combine track title and all artist ids to str to uniquely identify tracks with different track_id
    """

    return f"{track['title']}:a[{':'.join([artist['id'] for artist in track['artists']])}]"


def remove_dupe(playlist_id: str, client=SP_CLIENT):
    data = read_playlist(playlist_id)

    id_map = defaultdict(lambda: [])

    for index, item in enumerate(data):
        id_map[item["id"]].append({"position": index, **item})

    remove = []
    for x in id_map:
        if len(id_map[x]) == 1:
            continue

        remove.extend([{"uri": x, "positions": [i["position"]]} for i in id_map[x][:-1]])

    if remove:
        client.playlist_remove_specific_occurrences_of_items(playlist_id, remove)


def remove_same_track_dupe(playlist_id, *args, keep_last=False, client=SP_CLIENT, **kwargs) -> set:
    """
    Find duplicate tracks that don't have the same track_id and delete all but first or last one
    """

    reverse = keep_last

    playlist_items = read_playlist(playlist_id)

    id_item_map = {}
    unique_track_id_items_map = defaultdict(set)

    for x in playlist_items:
        id_item_map[x["id"]] = x
        unique_track_id_items_map[unique_track_str(x)].add(x["id"])

    to_delete_track_ids = set()
    for x in unique_track_id_items_map:
        if len(unique_track_id_items_map[x]) > 1:
            tracks = [id_item_map[track_id] for track_id in unique_track_id_items_map[x]]
            tracks.sort(key=lambda track: track["added_at"], reverse=reverse)

            to_delete_track_ids = to_delete_track_ids.union({track["id"] for track in tracks[1:]})

    if to_delete_track_ids:
        for chunk in chunks(to_delete_track_ids, 25):
            client.playlist_remove_all_occurrences_of_items(playlist_id, chunk)

    return to_delete_track_ids
