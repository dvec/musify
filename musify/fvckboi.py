import enum

try:
    from musify.config import SP_CLIENT
    from musify.lib import create_id_set, create_id_map, read_playlist_chunks, chunks
    from musify.playlists import Playlist
except Exception:
    from config import SP_CLIENT
    from lib import create_id_set, create_id_map, read_playlist_chunks, chunks
    from playlists import Playlist


class MatchPlaylist(str, enum.Enum):
    NEW_MUSIC_FRIDAY = "37i9dQZF1DX4JAvHpjipBk"


def auto_match_fvckboi(new_track_ids: set, client=SP_CLIENT):
    DE_PLAYLISTS = [
        Playlist.FVCKBOI_DE,
        Playlist.MODUS_MIO_MEGA,
        Playlist.DEUTSCHRAP_ROYAL_MEGA,
        Playlist.DEUTSCHRAP_BRANDNEU_MEGA,
        Playlist.DEUTSCHTRAP_MEGA,
        Playlist.RAP_IN_DEEP_MEGA,
        Playlist.DEUTSCHRAP_UNTERGRUND_MEGA,
    ]

    NON_DE_PLAYLISTS = [
        Playlist.FVCKBOI_NON_DE,
        Playlist.AMIGA,
        Playlist.TODAYS_TOP_HITS_MEGA,
        Playlist.TOP_HITS_MEGA,
        MatchPlaylist.NEW_MUSIC_FRIDAY,
    ]

    ## NON DE SOURCES
    non_de_tracks = []
    for playlist in NON_DE_PLAYLISTS:
        non_de_tracks.extend([x for x in read_playlist_chunks(playlist.value, client=client)])

    non_de_artists = set()
    for track in non_de_tracks:
        for artist in track["artists"]:
            non_de_artists.add(artist["id"])

    ## DE SOURCES
    de_tracks = []
    for playlist in DE_PLAYLISTS:
        de_tracks.extend([x for x in read_playlist_chunks(playlist.value, client=client)])

    de_artists = set()
    for track in de_tracks:
        for artist in track["artists"]:
            de_artists.add(artist["id"])

    ## NEW SOURCES
    new_tracks = [client.track(track_id) for track_id in new_track_ids]

    ## MATCH SOURCES
    new_non_de_tracks = set()
    new_de_tracks = set()
    new_unmatched_tracks = set()

    for track in new_tracks:
        track_id = track["id"]
        track_language_match = None
        for artist in track["artists"]:
            artist_id = artist["id"]

            if artist_id in non_de_artists and artist_id not in de_artists:
                if track_language_match is not None and track_language_match != "INTERNATIONAL":
                    track_language_match = None
                    break
                else:
                    track_language_match = "INTERNATIONAL"
            elif artist_id in de_artists and artist_id not in non_de_artists:

                if track_language_match is not None and track_language_match != "GERMAN":
                    track_language_match = None
                    break
                else:
                    track_language_match = "GERMAN"
            else:
                break

        if track_language_match == "INTERNATIONAL":
            new_non_de_tracks.add(track_id)
        elif track_language_match == "GERMAN":
            new_de_tracks.add(track_id)
        else:
            new_unmatched_tracks.add(track_id)

    return new_non_de_tracks, new_de_tracks, new_unmatched_tracks


def up_to_date_fvckboi(client=SP_CLIENT):
    source = create_id_map([x for x in read_playlist_chunks(Playlist.FVCKBOI.value, client=client)])

    for x in [
        (Playlist.FVCKBOI_DE, Playlist.FVCKBOI_DE_UP_TO_DATE),
        (Playlist.FVCKBOI_NON_DE, Playlist.FVCKBOI_NON_DE_UP_TO_DATE)
    ]:
        filter_playlist, dest_playlist = x
        print(dest_playlist.name)

        filter = create_id_set([x for x in read_playlist_chunks(filter_playlist.value, client=client)])
        dest = create_id_set([x for x in read_playlist_chunks(dest_playlist.value, client=client)])

        complete = set()
        add = []
        for k in source:
            if k not in filter:
                continue

            complete.add(k)

            if k in dest:
                continue

            add.append(k)

        merge = filter.union(dest)
        remove = [x for x in merge.difference(complete) if x in dest]

        if remove:
            for chunk in chunks(remove, 50):
                client.playlist_remove_all_occurrences_of_items(dest_playlist.value, chunk)

        if add:
            for chunk in chunks(add, 50):
                client.playlist_add_items(dest_playlist.value, chunk, position=0)

        print(len(add), len(remove), len(complete), len(filter), len(source))


def do_fvckboi():
    mega_playlist = Playlist.FVCKBOI_MEGA
    dest_playlists = [Playlist.FVCKBOI_NON_DE, Playlist.FVCKBOI_DE, Playlist.FVCKBOI_UNMATCHED]

    existing_ids = set()
    for playlist in dest_playlists:
        existing_ids.update(create_id_set([x for x in read_playlist_chunks(playlist.value)]))

    possible_new_ids = create_id_set([x for x in read_playlist_chunks(mega_playlist.value)])

    new_ids = possible_new_ids.difference(existing_ids)
    print(f"found {len(new_ids)} new tracks in fvckboi")

    if new_ids:
        _new_ids = auto_match_fvckboi(new_ids)

        for new_matched_ids, playlist in zip(
                _new_ids,
                [Playlist.FVCKBOI_NON_DE, Playlist.FVCKBOI_DE, Playlist.FVCKBOI_UNMATCHED]
        ):
            print(f"Adding {len(new_matched_ids)} track\s to {playlist.name}")
            for chunk in chunks(new_matched_ids, 50):
                SP_CLIENT.playlist_add_items(playlist.value, chunk, position=0)

    print("up to date fvckboi...")
    up_to_date_fvckboi()


def main(*args, **kwargs):
    do_fvckboi()


if __name__ == '__main__':
    main()
