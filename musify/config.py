from pathlib import Path

import spotipy
from envyaml import EnvYAML

PY_PATH = Path(__file__).parent
BASE_PATH = PY_PATH.parent
CACHE_PATH = BASE_PATH.joinpath("cache")
CACHE_PATH.mkdir(exist_ok=True)

TMP_PATH = CACHE_PATH.joinpath("tmp")
TMP_PATH.mkdir(exist_ok=True)

CONFIG = EnvYAML(yaml_file=BASE_PATH.joinpath("config", "config.yaml"), include_environment=False)

client_id = CONFIG["client_id"]
client_secret = CONFIG["client_secret"]
redirect_uri = CONFIG["redirect_uri"]
OATH_CACHE_PATH = CACHE_PATH.joinpath(".cache-dvec.tz")

SP_CLIENT = spotipy.Spotify(
    auth_manager=spotipy.SpotifyOAuth(
        client_id=client_id,
        client_secret=client_secret,
        redirect_uri=redirect_uri,
        cache_path=OATH_CACHE_PATH,
        open_browser=False,
        scope="playlist-modify-public playlist-read-private playlist-modify-private user-library-modify user-library-read playlist-read-collaborative"
    ),
    retries=5,
    status_retries=5,
    backoff_factor=4,
)

if not OATH_CACHE_PATH.exists():
    SP_CLIENT.me()

USER_ID = SP_CLIENT.me()["id"]
